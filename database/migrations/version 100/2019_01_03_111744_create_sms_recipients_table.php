<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_recipients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sms_id')->index();
            $table->integer('contact_id')->index();
            $table->timestamps();
            $table->foreign('sms_id')->references('id')->on('sms')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_recipients');
    }
}
