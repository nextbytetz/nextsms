<?php

//use Database\TableComment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateRolesTable extends Migration
{
//    use TableComment;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function(Blueprint $table)
        {
            $table->smallInteger('id', true);
            $table->string('name', 120);
            $table->text('description')->nullable();
            $table->smallInteger('isfree')->default(1);
            $table->smallInteger('isadministrative')->default(0);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
    }
}
