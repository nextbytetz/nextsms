<?php

use Illuminate\Database\Seeder;

class SendersTableSeeder extends Seeder
{
    use DisableForeignKeys,
        TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys('senders');
        $this->delete('senders');

        DB::table('senders')->insert([
            0 => [
                'name' => 'NEXTSMSDEMO'
            ]
        ]);
    }
}
