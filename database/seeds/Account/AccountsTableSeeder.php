<?php

use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{
    use DisableForeignKeys,
        TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys('accounts');
        $this->delete('accounts');

        DB::table('accounts')->insert([
            0 => [
              'type' => 'Master',
              'isAdmin' => '1'
            ],
            1 => [
                'type' => 'Sub',
                'isAdmin' => '0'
            ],
        ]);
    }
}
