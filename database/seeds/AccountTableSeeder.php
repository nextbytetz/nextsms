<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        $this->call(AccountsTableSeeder::class);
        $this->call(SendersTableSeeder::class);


        DB::commit();
    }
}
