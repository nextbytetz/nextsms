<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    use DisableForeignKeys,
        TruncateTable;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys("roles");
        $this->delete('roles');


        \DB::table('roles')->insert([
            0 =>
                [
                    'id' => 1,
                    'name' => 'Guest',
                    'description' => 'users visiting only the frontend of the app',
                    'isfree' => 1,
                    'isadministrative' => 0,
                    'created_at' => Carbon::now()
                ],
            1 =>
                [
                    'id' => 2,
                    'name' => 'Free Mode',
                    'description' => 'registered user',
                    'isfree' => 1,
                    'isadministrative' => 0,
                    'created_at' => Carbon::now()
                ],
            2 =>
                [
                    'id' => 3,
                    'name' => 'Standard Mode',
                    'description' => 'registered user with moderate privileges',
                    'isfree' => 0,
                    'isadministrative' => 0,
                    'created_at' => Carbon::now()
                ],
            3 =>
                [
                    'id' => 4,
                    'name' => 'Premium Mode',
                    'description' => 'registered user with full privileges',
                    'isfree' => 0,
                    'isadministrative' => 0,
                    'created_at' => Carbon::now()
                ],
            4 =>
                [
                    'id' => 5,
                    'name' => 'Administrator',
                    'description' => 'managers users of the app',
                    'isfree' => 1,
                    'isadministrative' => 1,
                    'created_at' => Carbon::now()
                ],
            5 =>
                [
                    'id' => 6,
                    'name' => 'Customer Care',
                    'description' => 'managers users of the app',
                    'isfree' => 1,
                    'isadministrative' => 1,
                    'created_at' => Carbon::now()
                ],
        ]);
        $this->enableForeignKeys('roles');


    }
}
