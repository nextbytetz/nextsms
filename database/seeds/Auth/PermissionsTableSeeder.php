<?php


use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    use DisableForeignKeys,
        TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('permissions');
        $this->delete('permissions');

        DB::table('permissions')->insert([
            0 => [
                'id' => 1,
                'name' => 'manage_app_users',
                'display_name' => 'Manage App Users',
                'description' => NULL,
                'ischecker' => 0,
            ],

            1 => [
                'id' => 2,
                'name' => 'create_multiple_sub_accounts',
                'display_name' => 'Create Multiple Sub Accounts',
                'description' => NULL,
                'ischecker' => 0,
            ],

            2 => [
                'id' => 3,
                'name' => 'have_multiple_sender_ids',
                'display_name' => 'Have Multiple Sender IDs',
                'description' => NULL,
                'ischecker' => 0,
            ],

            3 => [
                'id' => 4,
                'name' => 'assign_sender_id',
                'display_name' => 'Assign Sender ID',
                'description' => NULL,
                'ischecker' => 0,
            ],
        ]);
        $this->enableForeignKeys('permissions');
    }
}
