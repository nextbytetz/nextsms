<?php

use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    use DisableForeignKeys,
        TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->disableForeignKeys('permission_role');
        $this->delete('permission_role');


        \DB::table('permission_role')->insert([
            0 => [
                'id' => '1',
                'permission_id' => '1',
                'role_id' => '5'
            ],

            1 => [
                'id' => '2',
                'permission_id' => '1',
                'role_id' => '6'
            ],

            2 => [
                'id' => '3',
                'permission_id' => '2',
                'role_id' => '3'
            ],

            3 => [
                'id' => '4',
                'permission_id' => '2',
                'role_id' => '4'
            ],

            4 => [
                'id' => '5',
                'permission_id' => '2',
                'role_id' => '5'
            ],

            5 => [
                'id' => '6',
                'permission_id' => '2',
                'role_id' => '6'
            ],

            6 => [
                'id' => '7',
                'permission_id' => '3',
                'role_id' => '4'
            ],

            7 => [
                'id' => '8',
                'permission_id' => '3',
                'role_id' => '5'
            ],

            8 => [
                'id' => '9',
                'permission_id' => '3',
                'role_id' => '6'
            ],

            9 => [
                'id' => '10',
                'permission_id' => '4',
                'role_id' => '4'
            ],

            10 => [
                'id' => '11',
                'permission_id' => '4',
                'role_id' => '5'
            ],

            11 => [
                'id' => '12',
                'permission_id' => '4',
                'role_id' => '6'
            ],
        ]);
        $this->enableForeignKeys('permission_role');
    }
}
