<?php

Route::get('/debug', function () {
    return view('debug');
});

Route::get('lang/{lang}', 'LanguageController@swap');

/*
* Frontend Routes
* Namespaces indicate folder structure
*/
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.',  ], function () {
include_route_files(__DIR__.'/frontend/');
});

Route::group(['namespace' => 'Backend', 'as' => 'backend.',  ], function () {
    include_route_files(__DIR__.'/backend/');
});
Auth::routes();
//Route::get('email/verify', 'VerificationController@show')->name('verification.notice');
//Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
//Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
Route::get('/registered/{user}', 'Auth\RegisterController@showRegisteredForm')->name("auth.registered")->middleware('guest');
Route::get('/verification/{user}', 'Auth\LoginController@verification')->name("auth.verification");
Route::get('/account/confirm/{token}', 'Auth\ConfirmAccountController@confirm')->name("auth.account.confirm");
Route::post('account/confirm/resend/{user}', 'Auth\ConfirmAccountController@sendConfirmationEmail')->name("auth.account.confirm.resend");
