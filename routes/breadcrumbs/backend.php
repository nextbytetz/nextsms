<?php



Breadcrumbs::for('backend.sms.dashboard', function ($trail){
    $trail->push(('Home'), route('backend.sms.dashboard'));
});

Breadcrumbs::for('backend.contacts.all', function ($trail){
    $trail->parent('backend.sms.dashboard');
    $trail->push(('Contact'), route('backend.contacts.all'));
});

Breadcrumbs::for('backend.sms.compose', function ($trail){
    $trail->parent('backend.sms.dashboard');
    $trail->push(('Compose'), route('backend.sms.compose'));
});
