<?php

//include_route_files(__DIR__ . 'breadcrumbs');

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

Breadcrumbs::for('backend.dashboard', function ($trail){
    $trail->push(('Home'), route('backend.dashboard'));
});

Breadcrumbs::for('backend.contacts.all', function ($trail){
    $trail->parent('backend.dashboard');
    $trail->push(('Contacts'), route('backend.contacts.all'));
});

Breadcrumbs::for('backend.compose', function ($trail){
    $trail->parent('backend.dashboard');
    $trail->push(('Compose'), route('backend.compose'));
});

Breadcrumbs::for('backend.groups', function ($trail){
    $trail->parent('backend.dashboard');
    $trail->push(('Groups'), route('backend.groups'));
});

Breadcrumbs::for('backend.groups.add', function ($trail){
    $trail->parent('backend.dashboard');
    $trail->push(('Add'), route('backend.groups.add'));
});

Breadcrumbs::for('backend.index', function ($trail){
    $trail->parent('backend.dashboard');
    $trail->push(('Sent sms'), route('backend.index'));
});

Breadcrumbs::for('backend.profile', function ($trail){
    $trail->parent('backend.dashboard');
    $trail->push(('My Account'), route('backend.profile'));
});

Breadcrumbs::for('backend.group.show', function ($trail, $group){
    $trail->parent('backend.groups');
    $trail->push($group->name, route('backend.group.show', $group->id));
});


Breadcrumbs::for('backend.group.add.contacts', function ($trail, $group){
    $trail->parent('backend.groups');
    $trail->push($group->name, route('backend.group.add.contacts', $group->id));
});
