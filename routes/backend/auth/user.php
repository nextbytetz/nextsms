<?php
Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function(){

    Route::group(['middleware' => 'auth'], function(){

        Route::post('change_password', 'UserPasswordController@changePassword')->name('change_password');
    });

});
