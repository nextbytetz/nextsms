<?php
Route::group([], function () {


    Route::group(['middleware' => 'auth'], function () {

        Route::get('sms', 'SmsController@index')->name('index');
        Route::get('sms/compose', 'SmsController@compose')->name('compose');
        Route::post('sms/send','SmsController@send')->name('send');
        Route::get('sms/sent', 'SmsController@sentSmsDataTable')->name('sent');
        Route::get('sender', 'SenderController@senderIdTable')->name('sender');
    });

});
