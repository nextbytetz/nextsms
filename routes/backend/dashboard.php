<?php
Route::group([], function () {


    Route::group(['middleware' => 'auth'], function () {

        Route::get('dashboard', 'DashboardController@index')->name('dashboard')->middleware('verified');

        Route::get('dashboard/users', 'DashboardController@usersTable')->name('users.table');

    });

});
