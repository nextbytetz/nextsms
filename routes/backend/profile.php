<?php
Route::group([], function () {


    Route::group(['middleware' => 'auth'], function () {

        Route::get('profile', 'ProfileController@profile')->name('profile');
        Route::post('edit','ProfileController@getEditModal')->name('profile.edit');
        Route::patch('update','ProfileController@update')->name('profile.update');

    });

});
