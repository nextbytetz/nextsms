<?php

Route::group([], function () {


    Route::group(['middleware' => 'auth'], function () {

        //Groups Routes
        Route::get('groups', 'GroupController@index')->name('groups');

        Route::get('groups/table', 'GroupController@groups')->name('groups_table');

        Route::post('group/create', 'GroupController@create')->name('group.add');

        Route::get('groups/add/{group}', 'GroupController@addContactGroup')->name('group.add.contacts');
        Route::post('group/store/{group}', 'GroupController@storeContact')->name('group.store.contact');
        Route::get('groups/{group}', 'GroupController@show')->name('group.show');
        Route::get('group/view/{group}', 'GroupController@viewGroupDataTable')->name('group.view');

        Route::get('remove/{contact}/group/{group_id}', 'GroupController@removeContactGroup')->name('remove.contact');

        Route::get('delete/{group}', 'GroupController@destroy')->name('group.delete');
    });

});
