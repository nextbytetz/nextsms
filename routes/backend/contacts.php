<?php

Route::group([], function () {


    Route::group(['middleware' => 'auth', 'as' => 'contacts.'], function () {

        //Contacts Routes
        Route::get('contact/add', 'ContactController@showAddContactForm')->name('add');
        Route::post('contact/add', 'ContactController@add')->name('add.post');


        Route::get('contacts/all/data', 'ContactController@contactsIndexData')->name('all.data');
        Route::get('contact/all', 'ContactController@contactsIndex')->name('all');

        Route::get('contact/edit/{contact}', 'ContactController@edit')->name('edit.contact');
        Route::post('contact/edit_modal', 'ContactController@getEditModal')->name('edit_modal');
        Route::patch('contact/update/{contact}', 'ContactController@update')->name('update.contact');
        Route::delete('contact/delete/{contact}', 'ContactController@destroyContact')->name('delete');
        Route::post('contact/delete_modal', 'ContactController@getDeleteModal')->name('delete_modal');


        Route::post('contact/upload', 'ContactController@upload')->name('upload');

    });

});
