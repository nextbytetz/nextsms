<?php

Route::get('/',function (){
    return view('frontend.index');
})->name('home');

Route::get('pricing', function () {
    return view('frontend.pages.pricing');
})->name('pricing');

Route::get('about', function () {
    return view('frontend.pages.about');
})->name('about');

Route::get('contact', function () {
    return view('frontend.pages.contact');
})->name('contact');

Route::get('recover', function () {
    return view('frontend.pages.recoverpassword');
})->name('recover');



Route::get('/land',function (){
    return view('frontend.land');
})->name('land');

//
//Route::get('dashboard', function (){
//
//    return view('backend.index');
//})->name('dashboard')->middleware('verified');

