<?php

return [
    'language-picker' => [
        'language' => 'Lugha',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'en'    => 'Kiingereza',
            'sw'    => 'Kiswahili',
        ],
    ],

    'frontend-menu' =>[
        'home'   => 'Mwanzo',
        'pricing'=> 'Bei',
        'about'  => 'Kuhusu',
        'contact_us'=>'Wasiliana Nasi'
    ],

];
