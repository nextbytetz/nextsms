<?php

return [
    'email' => [
        'confirm_account' => [
            'line_1' => 'Click on the following button to confirm your NextSMS account registration.',
            'line_2' => 'If you’re having trouble clicking the button, copy and paste the URL below into your web browser: ',
        ],
        'passwords' => [
            'line_1' => "Here are your password reset instructions.",
            'line_2' => "A request to reset your password has been made. If you did not make this request, simply ignore this email. If you did make this request, please reset your password:",
            "button" => "Reset Password",
            "line_3" => "If the button above does not work, try copying and pasting the URL into your browser. If you continue to have problems, please feel free to contact us",
        ],
    ],
    'sms' => [
        'registered' => 'Your online account has been registered, please confirm your account via email and start registering your organization',
    ],
];
