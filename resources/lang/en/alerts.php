<?php
return [
    'auth' => [
        'logged-in' => 'You have Logged in Successfully',
        'logged-out' => 'You have Logged out Successfully',
        'not_confirmed' => 'Sorry, Your account has not been confirmed',
        'confirmation_sent' => 'Confirmation email has been sent to your email address',
        'change_password' => 'Success, Password has been changed',
        'forbidden' => 'Forbidden',
    ],
    'registration' => [
        'registered' => 'Success, your account has been created.',
    ],

];
