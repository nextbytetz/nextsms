<?php

return [
    'language-picker' => [
        'language' => 'Language',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'en'    => 'English',
            'sw'    => 'Swahili',
        ],
    ],

    'frontend-menu' =>[
        'home'   => 'Home',
        'pricing'=> 'Pricing',
        'about'  => 'About',
        'contact_us'=>'Contact Us'
    ],
];
