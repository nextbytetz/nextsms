<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',


    'login' => [
        'heading' => 'Login to your account',
        'placeholder' => [
            'email' => 'Email Address',
            'password' => 'Enter Password'
        ],
        'remember' => 'Remember Me',
        'forgot' => 'Forgot Password?',
        'login-button' => 'Login',
        'log-out-button' => 'Logout'
    ],
];
