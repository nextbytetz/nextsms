<header id="#home">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators col-4">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active" style="background-image:  url({{ url('img/photo-1510070112810-d4e9a46d9e91.jpeg') }})">
                <div class="carousel-caption d-none d-md-block col-md-4">
                    <h2 class="display-4">Welcome to NextSMS</h2>
                    <p class="lead">Where SMS World is powered by youself</p>
                </div>
            </div>
            <!-- Slide Two - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url({{ url('img/photo-1510070009289-b5bc34383727.jpeg') }})">
                <div class="carousel-caption d-none d-md-block col-md-4">
                    <h2 class="display-4">Reach your customers</h2>
                    <p class="lead">Have contacts and send them all information about your.</p>
                </div>
            </div>
            <!-- Slide Three - Set the background image for this slide in the line below -->
            <div class="carousel-item" style="background-image: url({{ url('img/photo-1509537257950-20f875b03669.jpeg') }})">
                <div class="carousel-caption d-none d-md-block col-md-4">
                    <h2 class="display-4">Own Sender ID</h2>
                    <p class="lead">Get known by your customers by your unique SMS name.</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev col-2" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next col-2" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="main-text hidden-xs col-md-4 offset-6">
        <div class="col-md-12" style="padding-top: 15px">
            {{--<h1>Static Headline And Content</h1>--}}
            {{--<div class="">--}}
                {{--<a class="btn btn-clear btn-sm btn-outline-primary" href="">Login</a> <a class="btn btn-clear btn-sm btn-outline-primary" href="">Registration</a></div>--}}
            @include('frontend.auth.login')
        </div>
    </div>
</header>
