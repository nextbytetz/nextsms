<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-static-top navbar-light navbar-border navbar-brand-center fixed-top col-sm-12">
    <div class="navbar-wrapper">
        <div class="navbar-container container center-layout">
            <ul class="nav navbar-nav mr-auto float-left">
                <li class="nav-item {{ \Request::route()->getName() == 'frontend.home' ? 'active' : '' }}">
                    <a class="navbar-brand" href="{{ route('frontend.home') }}">
                        <img class="brand-logo" alt="stack admin logo" src="{{ url("app-assets/images/logo/sms_black.png") }}">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="navbar-brand" href="{{ route('frontend.home') }}">
                        <h2 class="brand-text">{{ config('app.name') }}</h2>
                    </a>
                </li>
            </ul>

            <ul class="nav navbar-nav mr-auto float-right">
                <li class="nav-item">
                    <a class="nav-link" href="#home">{{ __('menu.frontend-menu.home') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('frontend.pricing') }}">{{ __('menu.frontend-menu.pricing') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('frontend.about') }}">{{ __('menu.frontend-menu.about') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">{{ __('menu.frontend-menu.contact_us') }}</a>
                </li>

                @include('partials.includes.lang')

            </ul>

        </div>
    </div>
</nav>
