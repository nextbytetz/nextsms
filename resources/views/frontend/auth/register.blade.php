@extends('layouts.frontend', ['title' => trans('label.register')])

@section('content')
    <div class="col-4 offset-md-4">
        <div class="card border-grey border-lighten-3 m-0">
            <div class="col-md-12 col-10 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 m-0">
                    <div class="card-header border-0">
                        <div class="card-title text-center">
                            <img src="{{ url("app-assets/images/logo/stack-logo-dark.png") }}" alt="branding logo">
                        </div>
                        <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                            <span>Create Account</span>
                        </h6>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form-horizontal form-simple" action="{{ url('register') }}" method="post" novalidate="">
                                {{ csrf_field() }}
                                <div class="form-group position-relative has-icon-left mb-1">
                                    <input id="first_name" type="text" class="form-control form-control-lg {{ $errors->has('first_name') ? ' is-invalid' : '' }}"  placeholder="First Name" name="first_name" value="{{ old('first_name') }}" required>
                                    <div class="form-control-position">
                                        <i class="ft-user"></i>
                                    </div>
                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group position-relative has-icon-left mb-1">
                                    <input id="last_name" type="text" class="form-control form-control-lg {{ $errors->has('last_name') ? ' is-invalid' : '' }}"  placeholder="Last Name" name="last_name" value="{{ old('last_name') }}" required>
                                    <div class="form-control-position">
                                        <i class="ft-user"></i>
                                    </div>
                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif

                                </div>
                                <div class="form-group position-relative has-icon-left mb-1">
                                    <input id="email" type="email" class="form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}"  placeholder="Your Email Address" name="email" value="{{ old('email') }}" required>
                                    <div class="form-control-position">
                                        <i class="ft-mail"></i>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                                </div>
                                <div class="form-group position-relative has-icon-left mb-1">
                                    <input id="phone_number" type="text" class="form-control form-control-lg {{ $errors->has('phone_number') ? ' is-invalid' : '' }}"  placeholder="Phone Number" name="phone_number" value="{{ old('phone_number') }}" required>
                                    <div class="form-control-position">
                                        <i class="ft-phone"></i>
                                    </div>
                                    @if ($errors->has('phone_number'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group position-relative has-icon-left">
                                    <input id="password" type="password" class="form-control form-control-lg {{ $errors->has('password') ? ' is-invalid' : '' }}"  placeholder="Enter Password" name="password" required>
                                    <div class="form-control-position">
                                        <i class="fa fa-key"></i>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group position-relative has-icon-left">
                                    <input id="password-confirm" type="password" class="form-control form-control-lg"  placeholder="Confirm Password" name="password_confirmation" data-validation-match-match="password" required >
                                    <div class="form-control-position">
                                        <i class="fa fa-key"></i>
                                    </div>
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i> Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
