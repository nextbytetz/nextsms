<div class="">
    <div class="card border-grey border-lighten-3 m-0">
        <div class="col-md-12 col-10 box-shadow-2 p-0">
            <div class="card-header border-0">
                <div class="card-title text-center">
                    <div class="p-1">
                        <img src="{{ url("app-assets/images/logo/sms_black.png") }}" alt="branding logo">
                    </div>
                </div>
                <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                    <span>{{ __('auth.login.heading') }}</span>
                </h6>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <form class="form-horizontal form-simple"  action="{{ url("login") }}" method="post" novalidate="">
                        {{ csrf_field() }}
                        <div class="form-group position-relative has-icon-left mb-1">
                            <input id="email" type="email" name="email" class="form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}"   placeholder="{{ __('auth.login.placeholder.email') }}" value="{{ old('email') }}" required>
                            <div class="form-control-position">
                                <i class="ft-mail"></i>
                            </div>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group position-relative has-icon-left mb-1">
                            <input id="password" type="password" name="password" class="form-control form-control-lg {{ $errors->has('password') ? ' is-invalid' : '' }}"  placeholder="{{ __('auth.login.placeholder.password') }}"  value="{{ old('password') }}"required>
                            <div class="form-control-position">
                                <i class="fa fa-key"></i>
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6 col-12 text-center text-md-left">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember-me"> {{ __('auth.login.remember') }}</label></div>
                            </div>
                            <div class="col-md-6 col-12 text-center text-md-right"><a href="{{ url("password/reset") }}" class="card-link">{{ __('auth.login.forgot') }}</a></div>
                        </div>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i>{{ __('auth.login.login-button') }}</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script src="{{ url("app-assets/vendors/js/forms/icheck/icheck.min.js") }}" type="text/javascript"></script>
    {{--<script src="{{ url("app-assets/vendors/js/forms/validation/jqBootstrapValidation.js") }}" type="text/javascript"></script>--}}
@endpush
