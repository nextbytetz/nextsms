@extends('frontend.layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email click the button to resend it') }}
                        {!! Form::open(['route' => ['auth.account.confirm.resend', $user->uuid]]) !!}
                        <center>
                        {!! Form::button(trans('label.resend_email'), ['class' => 'btn btn-primary', 'type'=>'submit', 'style' => 'border-radius: 6px;margin-bottom: 10px;']) !!}
                        </center>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
