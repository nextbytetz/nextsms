@extends('layouts.frontend', ['title' => trans('')])

@section('content')
    <div class="col-4 offset-md-4">
        <div class="card border-grey border-lighten-3 m-0">
            <div class="col-md-12 col-10 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 m-0">
                    <div class="card-header border-0">
                        <div class="card-title text-center">
                            <img src="{{ url("app-assets/images/logo/stack-logo-dark.png") }}" alt="branding logo">
                        </div>
                        <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                            <span>Reset Your Password</span>
                        </h6>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form class="form-horizontal form-simple" action="{{ route('password.update') }}" method="post" novalidate="">
                                {{ csrf_field() }}

                                <input type="hidden" name="token" value="{{ $token }}">

                                <div class="form-group position-relative has-icon-left mb-1">
                                    <input id="email" type="email" class="form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}"  placeholder="Your Email Address" name="email" value="{{ old('email') }}" required>
                                    <div class="form-control-position">
                                        <i class="ft-mail"></i>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif

                                </div>
                                <div class="form-group position-relative has-icon-left">
                                    <input id="password" type="password" class="form-control form-control-lg {{ $errors->has('password') ? ' is-invalid' : '' }}"  placeholder="Enter Password" name="password" required>
                                    <div class="form-control-position">
                                        <i class="fa fa-key"></i>
                                    </div>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group position-relative has-icon-left">
                                    <input id="password-confirm" type="password" class="form-control form-control-lg"  placeholder="Confirm Password" name="password_confirmation" required>
                                    <div class="form-control-position">
                                        <i class="fa fa-key"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i> Reset</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
