@extends('layouts.frontend', ['title' => trans('label.home')])

@push('css')
    <link href="{{ url("assets/vendors/frontend/css/bootstrap.min.css") }}">

    {{--Custom Style for header slider--}}
    <style>
        .carousel-item {
            height: 550px;
            min-height: 350px;
            background: no-repeat center center scroll;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .main-text
        {
            position: absolute;
            top: 50px;
            width: 96.66666666666666%;
            color: #FFF;
        }
    </style>
    @endpush

@section('content')

    @include('frontend.include.slider')


    <section id="contact">
        <div class="container-fluid">

            <div class="header">
                <h3>Contact Us</h3>
            </div>

            <div class="row">
            </div>
        </div>
    </section>

    @endsection

@push('scripts')
    <script src="{{ url("assets/vendors/frontend/js/bootstrap.bundle.min.js") }}"></script>
    {{--<script src="{{ url("assets/vendors/frontend/js/jquery-3.3.1.slim.min.js") }}"></script>--}}
    @endpush
