<!DOCTYPE html>
<html class="loading" lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="@yield('meta_description', 'Next Sms')">
    <meta name="author" content="@yield('meta_author', 'NextByte')">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')
    <title>{{ config('app.name') .' | '. $title }}</title>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url("favions/apple-icon-57x57.png")}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url("favions//apple-icon-60x60.png") }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url("favions/apple-icon-72x72.png") }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url("favions/apple-icon-76x76.png") }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url("favions/apple-icon-114x114.png") }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url("favions/apple-icon-120x120.png") }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url("favions/apple-icon-144x144.png") }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url("favions/apple-icon-152x152.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url("favions/apple-icon-180x180.png") }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ url("favions/android-icon-192x192.png") }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url("favions/favicon-32x32.png") }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url("favions/favicon-96x96.png") }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url("favions/favicon-16x16.png") }}">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url("favions/ms-icon-144x144.png") }}">
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
          rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url ("app-assets/css/vendors.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/app.css") }}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/core/menu/menu-types/vertical-menu.css") }}">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url("assets/css/style.css") }}">
    <!-- END Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/vendors/css/tables/datatable/datatables.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/vendors/css/tables/datatable/ext/select.dataTables.min.css") }}">
    <link type="text/css" href="{{ url("
    assets/vendors/jquery-datatables-checkboxes-1.2.11/css/dataTables.checkboxes.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("assets/vendors/select2-4.0.6-rc.1/dist/css/select2.min.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/plugins/forms/extended/form-extended.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/core/colors/palette-tooltip.css") }}">
@stack('css')
    {{--<link rel="stylesheet" type="text/css" href="{{ url("assets/vendors/jquery-datatables-checkboxes/css/dataTables.checkboxes.css")}} }}">--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ url ("app-assets/vendors/css/extensions/sweetalert.css") }}">--}}
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->

    <!--BEGIN Datatables CSS -->
    {{--<link rel="stylesheet"  type="text/css" href="{{ url("assets/vendors/datatables/DataTables-1.10.18/css/jquery.dataTables.min.css") }}">--}}
    {{--<link rel="stylesheet" type="text/css" href="{{ url("assets/vendors/datatables/Buttons-1.5.4/css/buttons.dataTables.min.css") }}">--}}
    <!-- END Datatables CSS-->
</head>
<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
        data-open="click" data-menu="vertical-menu" data-col="2-columns">

@include('backend.include.header')
@include('backend.include.sidebar')
<div class="app-content content">
    <div class="content-wrapper">

        {{ Breadcrumbs::render() }}
        @include('partials.includes.alerts')
        @yield('content')
    </div>
    </div>

<!-- BEGIN VENDOR JS-->
<script src="{{ url("app-assets/vendors/js/vendors.min.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
{{--<script src="{{ url("app-assets/vendors/js/extensions/jquery.knob.min.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/js/scripts/extensions/knob.js") }}" type="text/javascript"></script>--}}
<script src="{{ url("app-assets/vendors/js/extensions/unslider-min.js") }}" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/core/colors/palette-climacon.css") }}">
<link rel="stylesheet" type="text/css" href="{{ url("app-assets/fonts/simple-line-icons/style.min.css") }}">
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{ url("app-assets/js/core/app.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/js/core/app-menu.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/js/scripts/customizer.js") }}" type="text/javascript"></script>
<!-- END STACK JS-->
<script src="{{ url("app-assets/js/scripts/extensions/sweet-alerts.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/js/scripts/navs/navs.js") }}" type="text/javascript"></script>

<!-- Latest compiled and minified JavaScript -->
<!-- jQuery -->
{{--<script src="{{ url("js/app.js") }}"></script>--}}

<!-- DataTables -->
<script type="text/javascript" href="{{ url("jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js") }}"></script>
{{--<link rel="stylesheet" type="text/css" href="{{ url("app-assets/vendors/css/tables/datatable/datatables.min.css") }}">--}}
{{--<script src="{{ url("assets/vendors/jquery-datatables-checkboxes/js/dataTables.checkboxes.min.js") }}"></script>--}}
{{--<script src="{{ url("assets/vendors/datatables/Buttons-1.5.4/js/dataTables.buttons.min.js") }}"></script>--}}
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{ url("app-assets/vendors/js/tables/datatable/datatables.min.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/vendors/js/tables/datatable/ext/dataTables.select.min.js") }}" type="text/javascript"></script>
{{--<script src="{{ url("app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js") }}"--}}
        {{--type="text/javascript"></script>--}}
{{--<script src="{{ url("app-assets/vendors/js/tables/datatable/dataTables.select.min.js") }}"--}}
        {{--type="text/javascript"></script>--}}
{{--<script src="{{ url("app-assets/js/scripts/tables/datatables-extensions/datatable-select.js") }}"--}}
        {{--type="text/javascript"></script>--}}
<script src="{{ url("app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js") }}"
        type="text/javascript"></script>
<script src="{{ url("app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js") }}"
        type="text/javascript"></script>
<script src="{{ url("app-assets/vendors/js/tables/jszip.min.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/vendors/js/tables/pdfmake.min.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/vendors/js/tables/vfs_fonts.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/vendors/js/tables/buttons.html5.min.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/vendors/js/tables/buttons.print.min.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/vendors/js/tables/buttons.colVis.min.js") }}" type="text/javascript"></script>
<script src="{{ url("assets/vendors/select2-4.0.6-rc.1/dist/js/select2.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/js/scripts/tooltip/tooltip.js") }}" type="text/javascript"></script>
@stack('scripts')
</body>
</html>
