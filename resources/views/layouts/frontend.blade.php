<!DOCTYPE html>
<html class="loading" lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="@yield('meta_description', 'Next Sms')">
    <meta name="author" content="@yield('meta_author', 'NextByte')">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')
    <title>{{ config("app.name") . " | " . $title }}</title>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url("favions/apple-icon-57x57.png")}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url("favions//apple-icon-60x60.png") }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url("favions/apple-icon-72x72.png") }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url("favions/apple-icon-76x76.png") }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url("favions/apple-icon-114x114.png") }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url("favions/apple-icon-120x120.png") }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url("favions/apple-icon-144x144.png") }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url("favions/apple-icon-152x152.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url("favions/apple-icon-180x180.png") }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ url("favions/android-icon-192x192.png") }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url("favions/favicon-32x32.png") }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url("favions/favicon-96x96.png") }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url("favions/favicon-16x16.png") }}">
    <link rel="manifest" href="{{ url("favions/manifest.json") }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url("favions/ms-icon-144x144.png") }}">
    <meta name="theme-color" content="#ffffff">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i"
          rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/vendors.css") }}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/app.css") }}">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/plugins/forms/validation/form-validation.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/core/menu/menu-types/horizontal-menu.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/core/colors/palette-gradient.css") }}">
    <!-- END Page Level CSS-->
    @stack('css')
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url("assets/css/style.css") }}">
    <!-- END Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/vendors/css/extensions/toastr.css") }}">
    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/plugins/extensions/toastr.css") }}">
</head>
<body class="horizontal-layout horizontal-menu 2-columns   menu-expanded" data-open="click"
      data-menu="horizontal-menu" data-col="2-columns">
        @include('frontend.include.header')
<div class="app-content content">
    <div class="content-wrapper" style="padding-top: 70px">
        <div class="row justify-content-around">
        {{--@include('partials.includes.alerts')--}}
    </div>
        @yield('content')
    </div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="{{ url("app-assets/vendors/js/vendors.min.js") }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ url("app-assets/vendors/js/ui/prism.min.js") }}"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="{{ url("app-assets/vendors/js/extensions/jquery.knob.min.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/js/scripts/extensions/knob.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/vendors/js/extensions/unslider-min.js") }}" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{{ url("app-assets/css/core/colors/palette-climacon.css") }}">
<link rel="stylesheet" type="text/css" href="{{ url("app-assets/fonts/simple-line-icons/style.min.css") }}">
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="{{ url("app-assets/js/core/app-menu.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/js/core/app.js") }}" type="text/javascript"></script>
<script src="{{ url("app-assets/js/scripts/customizer.js") }}" type="text/javascript"></script>
<!-- END STACK JS-->
        {{--<script src="{{ url('app-assets/js/scripts/forms/validation/form-validation.js') }}" type="text/javascript"></script>--}}
        <script src="{{ url("app-assets/js/scripts/extensions/toastr.js") }}" type="text/javascript"></script>
        @stack('scripts')
</body>
</html>
