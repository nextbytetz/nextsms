{{--<div class="content-header row">--}}
    {{--<div class="content-header-left col-md-6 col-12 mb-2">--}}
        {{--<div class="row breadcrumbs-top">--}}
            {{--<div class="breadcrumb-wrapper col-12">--}}
                {{--<ol class="breadcrumb">--}}
                    {{--{{ Breadcrumbs::render() }}--}}
                {{--</ol>--}}
                {{--<ol class="breadcrumb">--}}
                    {{--<li class="breadcrumb-item">--}}
                        {{--<a href="index.html">--}}
                            {{--<i class="fa fa-home"></i> Home--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="breadcrumb-item"><span>Pages</span></li>--}}
                    {{--<li class="breadcrumb-item active"><span>Blank Page</span></li>--}}
                {{--</ol>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<h3 class="content-header-title mb-0">Breadcrumb Title</h3>--}}
    {{--</div>--}}
{{--</div>--}}
@if(count($breadcrumbs))
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">

                        @foreach ($breadcrumbs as $breadcrumb)

                            @if ($breadcrumb->url && !$loop->last)
                                <li class="breadcrumb-item"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                            @else
                                <li class="breadcrumb-item active">{ $breadcrumb->title }}</li>
                            @endif
                    </ol>
                </div>
            </div>
            <h3 class="content-header-title mb-0">{{ $breadcrumbs->title }}</h3>
            @endforeach
        </div>
    </div>
@endif
