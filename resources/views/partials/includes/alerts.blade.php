@if(session()->get('flash_success'))
<div class="alert alert-success" id="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            @if(is_array(json_decode(session()->get('flash_success'), true)))
                {!! implode('', session()->get('flash_success')->all(':message<br/>')) !!}
            @else
                {!! session()->get('flash_success') !!}
            @endif
        </div>
    @endif
    @if (session()->get('flash_info'))
        <div class="alert alert-info" id="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            @if(is_array(json_decode(session()->get('flash_info'), true)))
                {!! implode('', session()->get('flash_info')->all(':message<br/>')) !!}
            @else
                {!! session()->get('flash_info') !!}
            @endif
        </div>
    @endif
    @if (session()->get('flash_warning'))
        <div class="alert alert-warning" id="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            @if(is_array(json_decode(session()->get('flash_warning'), true)))
                {!! implode('', session()->get('flash_warning')->all(':message<br/>')) !!}
            @else
                {!! session()->get('flash_warning') !!}
            @endif
        </div>
    @endif
        @if($errors->any())
            <div class="alert alert-danger" role="alert" id="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                @foreach($errors->all() as $error)
                    {{ $error }}<br/>
                @endforeach
            </div>
    @endif
    @if (session()->get('flash_message'))
        <div class="alert alert-default" role="alert" id="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            @if(is_array(json_decode(session()->get('flash_message'), true)))
                {!! implode('', session()->get('flash_message')->all(':message<br/>')) !!}
            @else
                {!! session()->get('flash_message') !!}
            @endif
        </div>
    @endif


@push('scripts')
    <script>
        $(document).ready (function(){
            $("#alert").fadeTo(2000, 500).slideUp(500, function(){
                $("#alert").slideUp(500);
            });
        });
    </script>
@endpush
