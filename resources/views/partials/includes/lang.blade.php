@php
$flag = [
    'en' => 'gb',
    'sw' => 'tz',
];
@endphp
<li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link" id="dropdown-flag" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="{{ url("img/blank.gif") }}" class="flag-icon flag-icon-{{ $flag[app()->getLocale()] }}" alt="{{ __('menu.language-picker.langs.' . app()->getLocale()) }}"> {{ __('menu.language-picker.langs.' . app()->getLocale()) }}

        </a>
        <div class="dropdown-menu" aria-labelledby="dropdown-flag">
            @foreach (array_keys(config('locale.languages')) as $lang)
                <a class="dropdown-item" href="{{ url('lang/' . $lang) }}">
                    <img src="{{ url("img/blank.gif") }}" class="flag-icon flag-icon-{{ $flag[$lang] }}" alt="{{ __('menu.language-picker.langs.'.$lang) }}"> {{ __('menu.language-picker.langs.'.$lang) }}
                </a>
            @endforeach
        </div>
    </li>
