@extends('layouts.errors', ['title' => trans('Page Expired')])

@section('code', '419')
@section('title', __('Page Expired'))

@section('message', __('Sorry, your session has expired. Please refresh and try again.'))
