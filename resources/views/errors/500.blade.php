@extends('layouts.errors', ['title' => trans('Error')])

@section('code', '500')
@section('title', __('Error'))

@section('message', __('Whoops, something went wrong on our servers.'))
