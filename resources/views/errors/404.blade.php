@extends('layouts.errors', ['title' => trans('http.404.title')])

@section('code', __('http.404.code'))
@section('title', __('http.404.title'))

@section('message', __('http.404.description'))
