@extends('layouts.backend', ['title' => trans('label.account')])

@section('content')

<div class="card">
    <div class="card-header">
        <h2 class="card-title">My Account</h2>
    </div>
    <div class="card-content">
    <div class="card-body">



    <div class="tabs tabs-tertiary">
    <ul class="nav nav-tabs nav-top-border no-hover-bg nav-justified">
        <li class="nav-item">
            <a class="nav-link active" href="#info" data-toggle="tab" >Information</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#accounts" data-toggle="tab" >Accounts</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#sender" data-toggle="tab" >Sender IDs</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#settings" data-toggle="tab" >Settings</a>
        </li>
    </ul>
    </div>
                <div class="tab-content">
            <div class="tab-pane active in" id="info">
                @include('backend.profile.include.tabs.info')
            </div>


            <div class="tab-pane" id="accounts">
                @include('backend.profile.include.tabs.accounts')
            </div>



            <div class="tab-pane" id="sender">
                @include('backend.profile.include.tabs.sender_id')
            </div>



            <div class="tab-pane" id="settings">
                @include('backend.profile.include.tabs.settings')
            </div>
                </div>


            </div>
        </div>
    </div>

    @endsection


@push('scripts')
    <script>

    $(document).ready(function() {
    $('#sender_table').DataTable({
    processing: true,
    serverSide: true,
    ajax: '{!! route('backend.sender') !!}',
    columns: [
    { data: 'name' , name: 'name'},
    ]
    });
    });

    </script>
    @endpush
