<div class="modal fade text-left" id="editProfileForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-text-bold-600" id="myModalLabel3">Edit Account Information</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div id="edit_modal"></div>
        </div>
    </div>
</div>
@push('edit_profile_script')
    <script>
            $(document).on("click", "a.edit_profile", function() {
                let $id = $(this).attr("data-target-id");
                $.post("{!! route("backend.profile.edit") !!}", {'id' : $id}, function ($data) {
                    $("#edit_modal").empty();
                    $($data).prependTo("#edit_modal");
                }, "html").done(function() {
                    $("#editProfileForm").modal('show');
                    $(document).off('submit', 'form[name=edit_profile]').on('submit', 'form[name=edit_profile]',function($e) {
                        $e.preventDefault();
                        let $form = this;
                        let $data = $($form).serialize();
                        $($form).find(':input').each(function () {
                            let $name = $(this).attr('name');
                            $("input[name=" + $name + "]").closest(".form-group").removeClass("has-danger").find(".tag-danger").remove();
                        });
                        $.ajax({
                            data: $data,
                            dataType: "json",
                            method: "POST",
                            url: $($form).attr("action"),
                            beforeSend: function ($e) {
                                $(".btn-outline-primary").prop("disabled", true);
                            },
                            success: function ($data) {
                                if ($data.success) {
                                    $("#editContactForm").modal('hide');
                                    $("#contacts-table").DataTable().ajax.reload( function (json) {
                                        $(".btn-outline-primary").val(json.submitButton)
                                    });
                                    swal({
                                        icon: "success",
                                        title: "Success",
                                        text: 'Contact has been updated successfully',
                                        button: false,
                                        timer: 2000,
                                    })
                                }
                            },
                            error: function ($data) {
                                let $errors = $.parseJSON($data.responseText);
                                $.each($errors, function ($index, $value) {
                                    $("input[name=" + $index + "]").closest(".form-group").addClass("has-danger").find(".help-block").append("<p class='tag tag-danger'>" + $value + "</p>")
                                });
                            }
                        }).done(function () {

                        }).fail(function () {

                        }).always(function () {
                            $(".btn-outline-primary").prop("disabled", false);
                        });
                    });
                });
                return false;
            });
    </script>
@endpush
