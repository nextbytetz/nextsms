<div class="container-fluid">
    <div class="card-body">

                    <div class="row">
                        <div class="col-md-12" >
                            <div class="pull-right" >
                                <a href="#changePasswordModal" data-toggle="modal"   ><i class="ft-lock"></i>&nbsp;Change Password</a>&nbsp;&nbsp;
                                <a class ="edit_profile"  href="edit/{{ $user->id }}"  data-method="edit_profile" data-target-id="{{ $user->id }}"><i class="ft-user-check"></i>&nbsp;Edit</a>&nbsp;&nbsp;
                                <a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            </div>
                        </div>
                    </div>

<div class="col-lg-10">
<table class="table table-striped table-bordered">
    <tr>
        <th>Name </th>
        <td class="col-lg-3">{{ $user->first_name }} {{ $user->last_name }}</td>
    </tr>
    <tr>
        <th>Email </th>
        <td class="col-lg-3">{{ $user->email }}</td>
    </tr>
    <tr>
        <th>Phone Number</th>
        <td class="col-lg-3">{{ $user->phone_number }}</td>
    </tr>
    <tr>
        <th>Account Type </th>
        <td class="col-lg-3"></td>
    </tr>

</table>
    </div>
</div>
</div>
                    @include('backend.profile.include.change_password')
                    @include('backend.profile.include.edit_profile')
                    @push('scripts')
                        @stack('edit_profile_script')
                    @endpush
