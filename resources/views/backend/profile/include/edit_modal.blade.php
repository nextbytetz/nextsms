{{ Form::model($user, [ 'class'=>'form-horizontal form-simple', 'method' => 'PATCH', 'route' => ['backend.sms.profile.update ', $user->id], 'name' => 'edit_profile']) }}
<div class="modal-body">
    <div class="form-group">
        {{  Form::label('first_name', 'First Name')  }}
        {{  Form::text('first_name', null, ['class'=> 'form-control form-control-lg' , 'placeholder'=>'First Name', 'required']) }}
        <span class="help-block"></span>
    </div>
    <div class="form-group">
        {{  Form::label('last_name', 'Last Name')  }}
        {{  Form::text('last_name', null, ['class'=> 'form-control form-control-lg' , 'placeholder'=>'Last Name', 'required']) }}
        <span class="help-block"></span>
    </div>
    <div class="form-group">
        {{  Form::label('email', 'Email')  }}
        {{  Form::text('email', null, ['class'=> 'form-control form-control-lg' , 'placeholder'=>'email', 'required']) }}
        <span class="help-block"></span>
    </div>
    <div class="form-group">
        {{  Form::label('phone_number', 'Phone Number')  }}
        {{  Form::text('phone_number', null, ['class'=> 'form-control form-control-lg' , 'placeholder'=>'Phone Number', 'required']) }}
        <span class="help-block"></span>
    </div>
</div>
<div class="modal-footer">
    <input type="reset" class="btn btn-outline-danger btn-lg" data-dismiss="modal" value="CANCEL">
    {{ Form::submit('UPDATE', ['class'=>'btn btn-outline-primary btn-lg ']) }}
</div>
{{ Form::close() }}
