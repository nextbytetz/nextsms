{!! Form::open(['route' => ['backend.auth.change_password'],'method'=>'post',
'id' => 'update']) !!}

<!-- Modal -->
<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content col-md-9">
            <div class="modal-header">
                <h4 class="modal-title">Change Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">


                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">

                            {{--Left--}}
                            <div class="col-xs-12col-lg-12 col-md-12 col-sm-612 col-xs-12">
                                <div class="form-group">
                                    {!! Form::label('old_password', 'Old Password', ['class' => 'required']) !!}
                                    {!! Form::password('old_password', ['class' => 'form-control', 'autocomplete' => 'off', 'id' => 'old_password', 'required']) !!}
                                    {!! $errors->first('old_password', '<span class="badge badge-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">

                            {{--Left--}}
                            <div class="col-xs-12col-lg-12 col-md-12 col-sm-612 col-xs-12">
                                <div class="form-group">
                                    {!! Form::label('password', 'Password', ['class' => 'required_asterik']) !!}
                                    {!! Form::password('password', ['class' => 'form-control', 'autocomplete' => 'off', 'id' => 'new_password', 'required']) !!}
                                    {!! $errors->first('password', '<span class="badge badge-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">

                            {{--Left--}}
                            <div class="col-xs-12col-lg-12 col-md-12 col-sm-612 col-xs-12">
                                <div class="form-group">
                                    {!! Form::label('password', 'Confirm Password', ['class' => 'required_asterik']) !!}
                                    {!! Form::password('password_confirmation', ['class' => 'form-control', 'autocomplete' => 'off', 'id' => 'password_confirmation', 'required']) !!}
                                    {!! $errors->first('password_confirmation', '<span class="badge badge-danger">:message</span>') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                {!! Form::button('CHANGE PASSWORD', ['class' => 'btn btn-primary', 'type'=>'submit', 'style' => 'border-radius: 5px;']) !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

{!! Form::close() !!}
