@extends('layouts.backend', ['title' => trans('label.groups')])
@push('css')
    <link rel="stylesheet" type="text/css" href="{{ url("css/select2.css") }}">
@endpush
@section('content')

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h1 class="card-title" id="basic-layout-form">Add contacts to {{ $group->name }}</h1>
            </div>
            <div class="card-body">
                {{--{{ Form::model($group, [ 'class'=>'form', 'method' => 'POST', 'route' => ['backend.contacts.group.store.contact', $group]]) }}--}}
                <form class="form" action="{{ route('backend.group.store.contact', $group) }}" method="post">
                    {{ csrf_field() }}

                    <div class="form-body">
                        <div class="form-group">
                            <label for="contact">Select Contacts</label>

                            {{ Form::select('contact[]',$contacts, null, ['id'=> 'contact','class'=>'form-control contact ', 'multiple']) }}
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-chevron-circle-up"></i> Add
                        </button>
                    </div>
                </form>
{{--{{ Form::close() }}--}}
            </div>
        </div>
    </div>

@endsection


@push('scripts')

    {{--<script src="{{ url("js/select2.min.js") }}" type="text/javascript"></script>--}}

    {{--<script>--}}
        {{--$(document).ready(function() {--}}
            {{--function formatSelection(state) {--}}
                {{--return state.text;--}}
            {{--}--}}

            {{--function formatResult(state) {--}}
                {{--console.log(state)--}}
                {{--if (!state.id) return state.text; // optgroup--}}
                {{--let id = 'state' + state.id.toLowerCase();--}}
                {{--let label = $('<label></label>', { for: id })--}}
                    {{--.text(state.text);--}}
                {{--let checkbox = $('<input type="checkbox">', { id: id });--}}

                {{--return checkbox.add(label);--}}
            {{--}--}}

            {{--$('.contact').select2({--}}
                {{--placeholder : 'Choose Contact',--}}
                {{--closeOnSelect: false,--}}
                {{--formatResult: formatResult,--}}
                {{--formatSelection: formatSelection,--}}
                {{--escapeMarkup: function (m) {--}}
                    {{--return m;--}}
                {{--},--}}
                {{--// matcher: function(term, text, opt){--}}
                {{--//     return text.toUpper().indexOf(term.toUpperCase())>=0 || opt.parent("optgroup").attr("label").toUpperCase().indexOf(term.toUpperCase())>=0--}}
                {{--// }--}}
            {{--});--}}

        {{--});--}}
    {{--</script>--}}

    <script>
        $(document).ready(function() {
            $('.contact').select2({
                placeholder : 'Choose Contact'
            });
        });
    </script>

@endpush
