
    <div class="content-header-right">
        <button class="btn btn-outline-primary btn-min-width mr-1 mb-1"  type="button" data-toggle="modal" data-target="#addGroupForm"><i class="fa fa-user-circle-o"></i> Add Group</button>
    </div>


{{--Modal--}}
<div class="modal fade text-left" id="addGroupForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-text-bold-600" id="myModalLabel33">Add New Group</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form class="form-horizontal form-simple" action="{{ route('backend.group.add') }}" method="post">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        {{  Form::label('name', 'Group Name')  }}
                        {{  Form::text('name', null, ['class'=> 'form-control form-control-lg' , 'placeholder'=>'Group Name', 'required']) }}
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-danger btn-lg" data-dismiss="modal" value="CANCEL">
                    <input type="submit" class="btn btn-outline-primary btn-lg" value="ADD">
                </div>
            </form>
        </div>
    </div>
</div>
