@extends('layouts.backend', ['title' => trans('label.groups')])


@section('content')

    <div class="content-body">
        <section id="configuration">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                            <a href="{{ route('backend.groups') }}" class="btn btn-danger btn-min-width mr-1 mb-1 col-sm-1">Go Back</a>
                            {{--<a href="{{ route('backend.contacts.groups') }}" class="btn btn-danger btn-min-width mr-1 mb-1 col-sm-1 offset-12">Go Back</a>--}}
                        </div>
                            <h4 class="card-title">{{ $group->name }}</h4>
                        </div>
                        <table class="table table-striped table-bordered" width="100%" id="show_group">
                            <thead>
                            <tr>
                                {{--<th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>--}}
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Phone Number</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>


    @endsection

@push('scripts')


    <script>
        $(document).ready(function() {
            $('#show_group').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('backend.group.view', $group) !!}',
                'order': [[1, 'asc']],
                columns: [
                    {data: 'first_name', name: 'first_name'},
                    {data: 'last_name', name: 'last_name'},
                    {data: 'phone_number', name: 'phone_number'},
                    {data: 'remove', name: 'remove', searchable: false, orderable: false}
                ]
            });
        });
    </script>

    @endpush
