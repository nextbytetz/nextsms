@extends('layouts.backend', ['title' => trans('label.groups')])

@section('content')
    <div class="content-body">
        <section id="configuration">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                @include('backend.groups.includes.add')

                                <div class="content-header-right">
                                    <button class="btn btn-outline-danger btn-min-width mr-1 mb-1 delete"><i class="fa fa-bitbucket"></i> Delete</button>
                                </div>

                            </div>
                                <h4 class="card-title">Groups</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                </ul>
                            </div>
                        </div>

                        <table class="table table-striped table-bordered" width="100%" id="groups_table">
                            <thead>
                            <tr>
                                <th><input type="checkbox" name="select_all" value="1" id="select-all"></th>
                                <th>Group Name</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endsection

@push('scripts')

    <script>
        $(document).ready(function(){
        let table =$('#groups_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('backend.groups_table') !!}',
            'select': 'multi',
            'order': [[1, 'asc']],
            columns: [
                { data: 'checkbox', name: 'checkbox', 'width': '1%', searchable: false, orderable: false},
                { data:'name', name: 'name', 'width': '30%' },
                { data: 'actions', name: 'actions', 'width': '20%', searchable: false, orderable: false}
            ]
        });
        });
    </script>

    <script>
        $(document).ready(function () {


            $('#select-all').on('click', function (e) {
                if ($(this).is(':checked', true)) {
                    $(".sub").prop('checked', true);
                } else {
                    $(".sub").prop('checked', false);
                }
            });
        });

        let bool;
        $("input.checkbox").change(function() {
            bool = $(".checkbox:not(:checked)").length != 0;
            // enable/disable
            $(".delete").prop('disabled', bool);
        });

    </script>

    @endpush
