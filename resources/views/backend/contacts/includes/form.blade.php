<div class="form-group">
    {{  Form::label('first_name', 'First Name')  }}
    {{  Form::text('first_name', null, ['class'=> 'form-control form-control-lg' , 'placeholder'=>'First Name', 'required']) }}
    <span class="help-block"></span>
</div>
<div class="form-group">
    {{  Form::label('last_name', 'Last Name')  }}
    {{  Form::text('last_name', null, ['class'=> 'form-control form-control-lg' , 'placeholder'=>'Last Name', 'required']) }}
    <span class="help-block"></span>
</div>
<div class="form-group">
    {{  Form::label('phone_number', 'Phone Number')  }}
    {{  Form::text('phone_number', null, ['class'=> 'form-control form-control-lg' , 'placeholder'=>'Phone Number', 'required']) }}
    <span class="help-block"></span>
</div>

