{{ Form::model($contact, [ 'class'=>'form-horizontal form-simple', 'method' => 'DELETE', 'route' => ['backend.contacts.delete', $contact->id]]) }}
<div class="modal-body">
    <h2>Are you sure you want to delete {{ $contact->first_name }} {{ $contact->last_name }}?</h2>
</div>
<div class="modal-footer">
    <input type="reset" class="btn btn-outline-black btn-lg" data-dismiss="modal" value="CANCEL">
    {{ Form::submit('DELETE', ['class' => 'btn btn-outline-danger btn-lg']) }}
</div>
{{ Form::close() }}
