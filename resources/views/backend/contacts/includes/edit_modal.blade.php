{{ Form::model($contact, [ 'class'=>'form-horizontal form-simple', 'method' => 'PATCH', 'route' => ['backend.update.contact', $contact->id], 'name' => 'edit_contact']) }}
<div class="modal-body">
@include("backend.contacts.includes.form")
</div>
<div class="modal-footer">
    <input type="reset" class="btn btn-outline-danger btn-lg" data-dismiss="modal" value="CANCEL">
    {{ Form::submit('UPDATE', ['class'=>'btn btn-outline-primary btn-lg ']) }}
</div>
{{ Form::close() }}
