<div class="modal fade text-left" id="uploadContacts" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-text-bold-600" id="myModalLabel33">Upload Contacts</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form class="form-horizontal form-simple" action="{{ route("backend.contacts.upload") }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
<div class="modal-body">

<p>Sample of a excel file to be uploaded with your contacts</p>
        <div class="modal-content">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>0634212272</td>
                    </tr>
                    <tr>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>0732516278</td>
                    </tr>
                    </tbody>
                </table>
            </div>
    </div>
    <div class="content-header-right col-6">
        <input name="csvfile" type="file" class="form-control-file" id="csvfile">
    </div>
</div>

                <div class="modal-footer">
                    <input type="reset" class="btn btn-outline-danger btn-lg" data-dismiss="modal" value="CANCEL">
                    <input type="submit" class="btn btn-outline-primary btn-lg" value="UPLOAD">
                </div>
</form>

        </div>
    </div>
</div>
