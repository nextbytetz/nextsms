@push('delete_contact_script')
    <script>
        $(document).on("click","a.delete_contact", function (e) {
            e.preventDefault();

            let me = $(this),
                url = me.attr('href'),
                // name=me.attr('first_name')+' '+me.attr('last_name'),
                csrf_token = $('meta[name="csrf-token"]').attr('content');

            // var id = $(this).attr("data-target-id");
            swal({
                title: 'Are you sure ?',
                text: 'Once deleted, you will not be able to recover this contact!',
                icon: 'warning',
                dangerMode: true,
                buttons: {
                    cancel: true,
                    confirm: "Yes, Delete it"
                }
            }).then((result) => {
                if(result)
                {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            '_method': 'DELETE',
                            '_token': csrf_token
                        },
                        success: function () {
                            $('#contacts-table').DataTable().ajax.reload();
                            swal({
                                icon: 'success',
                                title: 'Success',
                                text: 'Contact has been deleted successfully',
                                button: false,
                                timer: 2000
                            });
                        },
                        error: function () {
                            swal({
                                icon: 'error',
                                title: 'Oops..',
                                text: 'There is error on deleting this contact'
                            });
                        }
                    })
                }
            });

        })
    </script>
@endpush
