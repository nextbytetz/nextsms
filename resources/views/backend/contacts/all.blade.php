@extends('layouts.backend' , ['title' => trans('label.contacts')])

@section('content')
    <div class="content-body">
        <section id="configuration">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Contacts</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                </ul>
                            </div>
                        </div>
    @include('backend.contacts.includes.add')
    {{--{!! $dataTable->table() !!}--}}
    <form id="frm-example" action="" method="POST">
    <table class="table table-striped table-bordered" id="contacts-table">
        <thead>
        <tr>
            {{--<th><span class="hide"></span><div class="icheck2 skin mass_select_all_wrap">--}}
                    {{--<label for="mass_select_all"></label><input type="checkbox" id="mass_select_all" data-to-table="contacts-table" name="checkbox"><label></label></div></th>--}}
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Actions</th>
        </tr>
        </thead>
    </table>
    </form>
    @include('backend.contacts.includes.edit')
    @include('backend.contacts.includes.delete')
                        </div>
                    </div>
                </div>
        </section>
    </div>
@endsection

@push('scripts')

    <script>
            $('#contacts-table').DataTable({
                processing: true,
                serverSide: true,
                // 'select': 'multi',
                // 'order': [[1, 'asc']],
                initComplete : function() {
                    initializeEditContact();
                },
                ajax: '{!! route('backend.contacts.all.data') !!}',
                columns: [
                    // {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                    { data: 'first_name', name: 'first_name' },
                    { data: 'last_name', name: 'last_name' },
                    { data: 'phone_number', name: 'phone_number' },
                    { data: 'actions', name: 'actions', orderable: false, searchable: false }
                ],

            });


    </script>

    @stack('edit_contact_script')
    @stack('delete_contact_script')
@endpush

