@extends('layouts.backend', ['title' => trans('label.dashboard')])

@push('css')
    <style>
    table.dataTable tr th.select-checkbox.selected::after {
    content: "✔";
    margin-top: -11px;
    margin-left: -4px;
    text-align: center;
    text-shadow: rgb(176, 190, 217) 1px 1px, rgb(176, 190, 217) -1px -1px, rgb(176, 190, 217) 1px -1px, rgb(176, 190, 217) -1px 1px;
    }
    </style>
    @endpush

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5">
                                <div class="media d-flex p-2">
                                    <div class="align-self-center">
                                            <a href="{{ route('backend.contacts.all') }}"><i class="icon-users font-large-1 blue-grey d-block mb-1"></i>
                                        <span class="text-muted text-right">Contacts</span></a>
                                    </div>
                                    <div class="media-body text-right">
                                        <span class="font-large-2 text-bold-300 primary">{{ $countNum }}</span>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5">
                                <div class="media d-flex p-2">
                                    <div class="align-self-center">
                                        <i class="icon-speech font-large-1 blue-grey d-block mb-1"></i>
                                        <span class="text-muted text-right">Remaining SMS</span>
                                    </div>
                                    <div class="media-body text-right">
                                        <span class="font-large-2 text-bold-300 danger">0</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5">
                                <div class="media d-flex p-2">
                                    <div class="align-self-center">
                                        <i class="icon-share-alt font-large-1 blue-grey d-block mb-1"></i>
                                        <span class="text-muted text-right">Sent SMS</span>
                                    </div>
                                    <div class="media-body text-right">
                                        <span class="font-large-2 text-bold-300 success">{{ $countSentSms }}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-12">
                                <div class="media d-flex p-2">
                                    <div class="align-self-center">
                                        <i class="icon-bulb font-large-1 blue-grey d-block mb-1"></i>
                                        <span class="text-muted text-right">Failed SMS</span>
                                    </div>
                                    <div class="media-body text-right">
                                        <span class="font-large-2 text-bold-300 warning">0</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @Admin

    <div class="content-body">
        <section class="configuration">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Users</h4>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered" id="users-table">
                        <thead>
                        <tr>
                            {{--<th></th>--}}
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                        </tr>
                        </thead>

                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>

    @endAdmin

@endsection

@push('scripts')

    <script>

        $('#users-table').DataTable({
            processing : true,
            serverSide: true,
            'order' : [[1, 'asc']],
            ajax: '{{ route('backend.users.table') }}',
            columns : [
                // { data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                { data: 'first_name', name: 'first_name' },
                { data: 'last_name', name: 'last_name' },
                { data: 'email', name: 'email' },
                { data: 'phone_number', name: 'phone_number' },
            ]

        })

    </script>

    <script>
        let example = $('#users-table').DataTable({
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'multi',
                selector: 'td:first-child'
            },
            order: [
                [1, 'asc']
            ]
        });
        example.on("click", "th.select-checkbox", function() {
            if ($("th.select-checkbox").hasClass("selected")) {
                example.rows().deselect();
                $("th.select-checkbox").removeClass("selected");
            } else {
                example.rows().select();
                $("th.select-checkbox").addClass("selected");
            }
        }).on("select deselect", function() {
            ("Some selection or deselection going on")
            if (example.rows({
                    selected: true
                }).count() !== example.rows().count()) {
                $("th.select-checkbox").removeClass("selected");
            } else {
                $("th.select-checkbox").addClass("selected");
            }
        });
    </script>

    @endpush
