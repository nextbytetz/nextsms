@extends('layouts.backend' , ['title' => trans('label.sms.compose')])
@push('css')

    <link rel="stylesheet" type="text/css" href="{{ url("app-assets/vendors/css/forms/selects/select2.min.css") }}">

@endpush
@section('content')

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">Compose Sms</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">

                </div>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">

                    <form class="form" action="{{ route("backend.send") }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-body">
                            <div class="form-group">
                                <label for="contact">Sender ID:</label>

                                {{ Form::select('sender',$senders, null, ['id'=> 'hide_search','class'=>'hide-search form-control col-md-3 sender', 'single'=> true ]) }}
                            </div>

                            <div class="row">
<div class="col-md-6">
    <label for="contact">Send to</label>
                            <div class="form-group">

                                {{ Form::select('contact[]',$contacts, null, ['data-placeholder' => 'Choose Contact','id'=> 'programmatic-multiple','class'=>'select2 js-example-programmatic-multi form-control col-md-8 ', 'multiple']) }}
                                <div class="btn-group btn-group-sm" role="group" aria-label="Programmatic setting and clearing Select2 options">

                                    <button type="button" class="js-programmatic-multi-clear btn btn-outline-primary">
                                        Clear
                                    </button>
                                </div>
                            </div>

                            </div>
                                {{--<div class="col-md-6">--}}

                                    {{--<div class="form-group">--}}


                                        {{--{{ Form::select('contact[]',$group, null, ['id'=> 'contact','class'=>'form-control group ', 'multiple']) }}--}}



                                    {{--</div>--}}
                                {{--</div>--}}
                        </div>

                            {{--<div class="form-group">--}}
                            {{--<label>Variables: </label>--}}
                                {{--<div>--}}
                                    {{--<button class="btn btn-info" id="add-tagging" type="button">Add { first_name }</button>--}}
                                    {{--<button class="btn btn-info add-tagging" type="button">{ last_name }</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <fieldset class="mb-1">
                                <div class="form-group">
                                    <label >Message</label>
                                    <textarea name="message" class="form-control" data-tags-input-name="add-box" id="message"
                                              placeholder="Enter Message.." rows="5"></textarea>
                                </div>
                                <div class="row">
                                    <p><span class="col-3" id="remaining">160 Characters remaining</span>  <span class="col-3" id="messages">1 Message(s)</span>
                                    </p>
                                </div>
                            </fieldset>
                        </div>

                        <div class="form-actions">

                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-check-square-o"></i> Send
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')

    <script src="{{ url("app-assets/vendors/js/forms/select/select2.full.min.js") }}" type="text/javascript"></script>

    <script src="{{ url("app-assets/js/scripts/forms/select/form-select2.js") }}" type="text/javascript"></script>

    <script>
        $(document).ready(function() {
            $('#programmatic-multiple').select2({
                placeholder : 'Choose Contact'
            });
        });
    </script>
    {{--<script>--}}
        {{--$(document).ready(function() {--}}
            {{--$('.group').select2({--}}
                {{--placeholder : 'Choose Group'--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}

    <script>
        let $remaining = $('#remaining'),
            $messages = $remaining.next();

        $('#message').keyup(function(){
            let chars = this.value.length;
            let messages = Math.ceil(chars / 160),
                remaining = messages * 160 - (chars % (messages * 160) || messages * 160);
            $remaining.text(remaining + ' Characters remaining');
            $messages.text(messages + ' Message(s)');
            if (chars > 160){
                let messages = Math.ceil(chars / 153),
                    remaining = messages * 153 - (chars % (messages * 153) || messages * 153);
                $remaining.text(remaining + ' Characters remaining');
                $messages.text(messages + ' Message(s)');
            }

        });
    </script>
@endpush
