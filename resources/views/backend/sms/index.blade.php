@extends('layouts.backend' , ['title' => trans('label.sms.sent')])



@section('content')
    <div class="content-body">
        <section id="configuration">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Sent SMS</h4>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered" width="100%" id="sms-table">
                            <thead>
                            <tr>
                                <th>Message</th>
                                <th>Contacts</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <script>
        $('#sms-table').DataTable({
            processing: true,
            serverSide: true,
            'order': [[0, 'des']],
            ajax: '{!! route('backend.sent') !!}',
            columns: [
                { data:'body', name:'body' },
                { data: 'contacts', name: 'contacts'},
                { data:'time', name:'time'}
            ]
        })
    </script>


    <script>
        $(document).ready(function() {
            let table = $('#sms-table').DataTable();

            $('#sms-table tbody').on('click', 'tr', function () {
                let data = table.row( this ).data();
                alert( 'You clicked on '+data[0]+'\'s row' );
            } );
        } );
    </script>
    @endpush
