
    <div class="sidebar">
        <div class="sidebar-content email-app-sidebar d-flex">
            <div class="email-app-menu col-md-12 card d-none d-lg-block">
                <div class="form-group form-group-compose text-center">
                    <a type="button" href="{{ route('backend.sms.compose') }}" class="btn btn-danger btn-block my-1"><i class="ft-mail"></i> Compose</a>
                </div>
                <h6 class="text-muted text-bold-500 mb-1">Messages</h6>
                <div class="list-group list-group-messages">
                    <a href="{{ route('backend.sms.index') }}" class="list-group-item {{ \Request::route()->getName() == 'backend.sms.index' ? 'active' : '' }} border-0">
                        <i class="ft-inbox mr-1"></i> Sent
                    </a>
                    <a href="#" class="list-group-item list-group-item-action border-0"><i class="ft-file mr-1"></i> Draft</a>
                    <a href="#" class="list-group-item list-group-item-action border-0"><i class="ft-star mr-1"></i> Starred<span class="badge badge-danger badge-pill float-right">3</span> </a>
                    <a href="#" class="list-group-item list-group-item-action border-0"><i class="ft-trash-2 mr-1"></i> Trash</a>
                </div>
            </div>
        </div>
    </div>

