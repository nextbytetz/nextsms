<div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
        <li class="nav-item">
            <a class="navbar-brand" href="{{ route('backend.dashboard') }}">
                <img class="brand-logo" alt="stack admin logo" src="{{ url("app-assets/images/logo/white_sms.png") }}">
                <h2 class="brand-text">{{ config("app.name") }}</h2>
            </a>
        </li>
        <li class="nav-item d-md-none">
            <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a>
        </li>
    </ul>
</div>
