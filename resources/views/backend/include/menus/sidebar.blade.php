<?php $sms = new \App\Repositories\SMS\SmsRepository();
//dd($sms->count());
?>


<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header">
                <span>General</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right"
                                       data-original-title="General"></i>
            </li>
            <li class="{{ \Request::route()->getName() == 'backend.dashboard' ? 'active' : '' }}"><a class="menu-item" href="{{ route('backend.dashboard') }}"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a></li>
            <li class="{{ \Request::route()->getName() == '' ? 'active' : '' }}"><a class="menu-item" href="#"><i class="ft-mail"></i><span class="menu-title" data-i18n="">SMS</span></a>
                <ul class="menu-content">
                    <li class="{{ \Request::route()->getName() == 'backend.compose' ? 'active' : '' }}"><a class="menu-item" href="{{ route('backend.compose') }}"><i class="ft-edit-3"></i> <span class="menu-title" data-i18n=""> Compose</span></a>
                    </li>
                    <li class="{{ \Request::route()->getName() == 'backend.index' ? 'active' : '' }}"><a class="menu-item" href="{{ route('backend.index') }}"><i class="ft-check-square"></i> <span class="menu-title" data-i18n=""> Sent <div class="badge badge-pill badge-danger badge-glow">{{ $sms->sentSms()->count()  }}</div>
</span></a>
                    </li>

                </ul>
            </li>
            <li class="{{ \Request::route()->getName() == '#' ? 'active' : '' }}"><a href="#"><i class="ft-phone"></i><span class="menu-title" data-i18n="">Contacts</span></a>
                <ul class="menu-content">
                    <li class="{{ \Request::route()->getName() == 'backend.all' ? 'active' : '' }}"><a class="menu-item" href="{{ route('backend.contacts.all') }}"><i class="ft-book"></i> <span class="menu-title" data-i18n="">All Contacts</span></a>

                    </li>
                    <li class="{{ \Request::route()->getName() == 'backend.groups' ? 'active' : '' }}"><a class="menu-item" href="{{ route('backend.groups') }}"><i class="ft-globe"></i> <span class="menu-title" data-i18n="">Groups</span></a>
                    </li>
                </ul>
            </li>

            <li class=" navigation-header">
                <span>Settings</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right"
                                       data-original-title="Settings"></i>
            </li>
    <li class="{{ \Request::route()->getName() == 'backend.profile' ? 'active' : '' }}"><a class="menu-item" href="{{ route('backend.profile') }}"><i class="ft-user"></i><span class="menu-title" data-i18n="">My Account</span></a></li>

    <li class=" nav-item"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="icon-logout"></i><span class="menu-title" data-i18n=""> {{ __('auth.login.log-out-button') }}</span></a>
        <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
            {{ csrf_field() }}
        </form>
    </li>

</ul>
