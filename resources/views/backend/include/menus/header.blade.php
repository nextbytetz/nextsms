<div class="navbar-container content">
    <div class="collapse navbar-collapse" id="navbar-mobile">
        @include('backend.include.navs.header.left')
        @include('backend.include.navs.header.right')
    </div>
</div>
