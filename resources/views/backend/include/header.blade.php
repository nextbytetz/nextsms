<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        @include('backend.include.incs.header')
        @include('backend.include.menus.header')
    </div>
</nav>
