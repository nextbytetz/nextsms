@component('mail::message')
# Introduction

Hello {{ $user->name }},<br>
Welcome to NextSMS.

@component('mail::button', ['url' => ''])
Get in NextSMS now to see our App.
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
