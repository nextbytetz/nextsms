<?php

namespace App\Rules\Backend\Contact;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;
use File;

class ValidFileImported implements Rule
{

    protected $request;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->request = new Request();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = File::extension($this->request->file()->getClientOriginalName());

        if($value == "xlsx" || $value == "xls" || $value == "csv"){
            return true;
            }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The format of the file is invalid';
    }
}
