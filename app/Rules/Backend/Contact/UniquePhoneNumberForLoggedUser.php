<?php

namespace App\Rules\Backend\Contact;
//use App\Models\Contact\Contact;
use App\Repositories\Backend\Contact\ContactRepository;
use function GuzzleHttp\Psr7\str;
use Illuminate\Contracts\Validation\Rule;
//use Illuminate\Support\Facades\DB;

class UniquePhoneNumberForLoggedUser implements Rule
{

    protected $phone;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->phone = new ContactRepository();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $formatted = str_replace_first('0', '+255', $value);
        $phone = $this->phone->phoneNumbersForLoggedUser($formatted)->get();
        return empty($phone[0]) ? true : false;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This :attribute is already added to your contacts';
    }
}
