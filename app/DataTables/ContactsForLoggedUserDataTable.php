<?php

namespace App\DataTables;

use App\Models\Contact\Contact;
use Yajra\DataTables\Services\DataTable;

class ContactsForLoggedUserDataTable extends DataTable
{

    /**
     * Build DataTable class.
     *
     * @param $contact
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($contact)
    {
        return datatables($contact)
            ->addColumn('check','<input type="checkbox" name="" value="">')
            ->addColumn('action', function (Contact $contact) {
                return $contact->action_buttons;
            }
            );
    }

    /**
     * Get query source of dataTable.
     *
     * @param Contact $contact
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Contact $contact)
    {
//        return $this->query()->where('user_id',access()->id())
//                                ->orderBy('first_name','ASC');
        return $contact->newQuery()->select('first_name','last_name','phone_number')
                                 ->where('user_id',access()->id())
                                 ->orderBy('first_name','ASC');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px'])
                    ->parameters([
                        'dom'          => 'Blfrtip',
                        'buttons'      =>  ['print']
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'first_name',
            'last_name',
            'phone_number'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ContactsForLoggedUser_' . date('YmdHis');
    }

}
