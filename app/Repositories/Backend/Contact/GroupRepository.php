<?php
/**
 * Created by PhpStorm.
 * User: reubenwedson
 * Date: 2/8/19
 * Time: 2:03 PM
 */

namespace App\Repositories\Backend\Contact;


use App\Models\Contact\Contact;
use App\Models\Contact\Group;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property  contact
 */
class GroupRepository extends BaseRepository
{

    const MODEL = Group::class;

    public function create(array $data)
    {
        $group = DB::transaction(function () use ($data){
            $this->query()->create([
                'name' => $data['name'],
                'user_id' => access()->id()
            ]);
            $group = $this->query()->where('name', $data['name'])->first();
            access()->user()->groups()->attach($group->id, ['user_id' => access()->id()]);
        });

        return $group;
    }

    public function update(Model $model, array $data)
    {
        $group = DB::transaction(function () use ($model, $data){
            $model->update([
                'name' => $data['name'],
            ]);
        });

        return $group;
    }

    public function destroyGroup($id)
    {
        DB::transaction(function () use ($id){
            $group = $this->find($id);
            $group->delete();
        });
    }

    public function getGroupsForLoggedUser()
    {
        return access()->user()->groups;
    }


    public function pluckGroup()
    {
        return access()->user()->groups->pluck('name','id');
    }

    public function viewGroupContacts($group)
    {
        return $group->contact;
    }

    public function storeContactGroup(Model $group, array $input)
    {
        return DB::transaction( function () use ($group, $input)
        {
            $contacts =  $input['contact'];

            foreach ($contacts as $contact){
                $group->contact()->attach($contact);
            }
        });
    }

    public function deleteGroup($group)
    {
        $group = $this->query()->where('id', $group)->first();
        return DB::transaction( function () use ($group) {
            foreach ($group->contact as $contact) {
                $group->contact()->detach($contact);
            }
            access()->user()->groups()->detach($group);
            $group->delete();
        });

    }

    public function removeContact($contact, $group)
    {
        $group = $this->query()->where('id', $group)->get();
        dd($group);
        DB::transaction( function () use ($contact) {

//            $group = $this->find($groupId);
//            $group->contact()->detach($contact);
        });
    }
}
