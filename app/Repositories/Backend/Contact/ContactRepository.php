<?php
namespace App\Repositories\Backend\Contact;

use App\Models\Contact\Contact;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
class ContactRepository extends BaseRepository
{
   const MODEL = Contact::class;

    /**
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model|mixed
     * @throws \Throwable
     */
    public function create(array $data)
    {
        $input = ['phone_number' => phone($data['phone_number'], $country = ['TZ'], $format = 'E164')];
//        $this->checkIfPhoneIsUnique($input,'phone_number','1');
        return DB::transaction(function () use ($data, $input) {
            $this->query()->create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'phone_number'=>$input['phone_number'],
                'user_id'=>access()->id()
            ]);

        });

    }

    /**
     * @param Model $contact
     * @param array $input
     * @return mixed
     */
    public function update(Model $contact, array $input)
    {
        $data = ['phone_number' => phone($input['phone_number'], $country = ['TZ'], $format = 'E164')];
        return DB::transaction( function () use ($input, $contact, $data){
            $contact->update([
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'phone_number' => $data['phone_number'],
            ]);
        });
    }


    public function getContactsForLoggedUser()
    {
         return $this->query()->where('user_id',access()->id());
    }

    public function getNames()
    {
        return $this->query()->select('first_name')->where('user_id',access()->id());    }

    public function destroyContact($id)
    {
        DB::transaction( function () use ($id){
           $contact = $this->find($id);
           $contact->delete();
        });
    }

    public function getNumberOfContactsForLoggedUser()
    {
        return access()->user()->contacts->count();
    }

    public function phoneNumbersForLoggedUser($phone)
    {
        return $this->query()->where('user_id', access()->id())->where('phone_number',$phone);
    }

    public function pluckContact()
    {
        return access()->user()->contacts->pluck('full_name','phone_number');
    }

    public function getFirstNameByNumber($contact)
    {
        return $this->query()->select(['first_name'])->where("phone_number",$contact)->first();
    }
}
