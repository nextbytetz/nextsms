<?php

namespace App\Repositories\Backend\SMS;

use App\Models\SMS\Sender;
use App\Repositories\BaseRepository;

class SenderRepository extends BaseRepository
{
    const MODEL = Sender::class;


    public function senderId()
    {
        return access()->user()->senders->pluck('sender_id', 'sender_id');
    }

    public function getSenderIds()
    {
        return access()->user()->senders;
    }
}
