<?php
namespace App\Repositories\SMS;

use App\Jobs\Backend\SMS\SendSms;
use App\Models\Contact\Group;
use App\Models\SMS\Sms;
use App\Repositories\Backend\Contact\ContactRepository;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

class SmsRepository extends BaseRepository
{
    const MODEL = Sms::class;


    public function sendSms($inputs)
    {
        return DB::transaction(function () use ($inputs) {
            $contacts = $inputs['contact'];
            dd($contacts);

            foreach ($contacts as $contact) {

//                $contact_repo = new ContactRepository();
//                $name = $contact_repo->getFirstNameByNumber($contact);

//                $message = ['message' => $name->first_name.", ".$inputs['message']];
//                dd($message);

//                SendSms::dispatch($sender ,$contact, $message, access()->id());

                SendSms::dispatch($inputs['sender'], $contact, $inputs['message'], access()->id());
            }
            return true;
        });
    }

    public function sentSms()
    {
        $sent = $this->query()
                    ->select(
//                        DB::raw( "distinct(sms_recipients.sms_id)"),
                        DB::raw("distinct(sms_recipients.sms_id) as user, count(sms_recipients.contact_id )AS contacts"),
                        DB::raw( "body"),
                        DB::raw( "sms.created_at AS time")
                    )
                    ->join('sms_recipients', "sms_id", "=", "sms.id")
                    ->groupBy(['body', 'sms_recipients.sms_id', 'sms.created_at'])
                    ->where("sms.user_id",access()->id())
        ->orderBy("sms.created_at", "desc");

        return $sent;

    }

    public function countSentSmsByLoggedUser()
    {
        return $this->query()->where('user_id', access()->id())->count();
    }
}
