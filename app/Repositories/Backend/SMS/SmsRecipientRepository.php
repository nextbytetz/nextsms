<?php
namespace App\Repositories\Backend\SMS;

use App\Models\SMS\SmsRecipient;
use App\Repositories\BaseRepository;

class SmsRecipientRepository extends BaseRepository
{
    const MODEL = SmsRecipient::class;
}
