<?php

namespace App\Repositories\Auth;

use App\Exceptions\GeneralException;
use App\Jobs\SendWelcomeEmail;
use App\Models\Auth\User;
use App\Notifications\Auth\UserNeedsConfirmation;
use App\Repositories\BaseRepository;
use Couchbase\UserSettings;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UserRepository extends BaseRepository
{
    /**
     * @return string
     */
    const MODEL = User::class;


    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        $input = ['phone_number' => phone($data['phone_number'], $country = ['TZ'], $format = 'E164')];
//        $this->checkIfPhoneIsUnique($input,'phone_number',1,'id');
        $user = DB::transaction(
            function () use ($data, $input) {

                 return $user = $this->query()->create([
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'email' => $data['email'],
                    'phone_number' => $input['phone_number'],
                    'password' => Hash::make($data['password']),
                    'confirmation_code' => md5(uniqid(mt_rand(), true))

                ]);
        });
        /* Send confirmation email */
        $user->notify(new UserNeedsConfirmation());
        return $user;
    }

    /**
     * @param $token
     * @return mixed
     */
    public function findByConfirmationToken($token)
    {
        return $this->query()->where('confirmation_code', $token)->first();
    }

    /**
     * @param $token
     * @return mixed
     * @throws GeneralException
     */
    public function confirmAccount($token)
    {
        $user = $this->findByConfirmationToken($token);

        if ($user->confirmed == 'true') {
            throw new GeneralException(__('exceptions.auth.confirmation.already_confirmed'));
        }

        if ($user->confirmation_code == $token) {
            $user->confirmed = 't';
            $user->save();
            return access()->login($user);
        }
        throw new GeneralException(trans('exceptions.auth.confirmation.mismatch'));
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function changePassword(array $input)
    {
        $user = access()->user();
        $user->update(['password' => bcrypt($input['password'])]);
        return $user;
    }

}
