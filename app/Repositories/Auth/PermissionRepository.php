<?php

namespace App\Repositories\Auth;

use App\Models\Auth\Permission;
use App\Repositories\BaseRepository;

class PermissionRepository extends BaseRepository
{
    const  MODEL = Permission::class;

    public  function  getAll()
    {
        return $this->query()->select(['id', 'display_name'])->where("ischecker", 0)->get();

    }
}
