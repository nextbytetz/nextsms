<?php

namespace App\Repositories\Auth;

use App\Models\Auth\Account;
use App\Repositories\BaseRepository;

class AccountRepository extends BaseRepository
{
   const MODEL = Account::class;
}
