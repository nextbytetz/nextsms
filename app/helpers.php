<?php

if (! function_exists('flashAlert')){

    function flashAlert($type, $message)
    {
        Session::put($type, $message);
    }
}

if (! function_exists('include_route_files')) {

    /**
     * Loops through a folder and requires all PHP files
     * Searches sub-directories as well.
     *
     * @param $folder
     */
    function include_route_files($folder)
    {
        try {
            $rdi = new recursiveDirectoryIterator($folder);
            $it = new recursiveIteratorIterator($rdi);

            while ($it->valid()) {
                if (! $it->isDot() && $it->isFile() && $it->isReadable() && $it->current()->getExtension() === 'php') {
                    require $it->key();
                }

                $it->next();
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}


if  (! function_exists('access') ){


    function access(){
        return app('access');
    }
}

if (!function_exists('max_doc_size_string')) {
    function max_logo_size_string() {
        return '4MB';
    }
}

if (!function_exists('max_doc_size_string')) {
    function max_doc_size_string() {
        return '3MB';
    }
}

