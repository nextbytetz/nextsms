<?php

namespace App\Http\Requests\Backend\Auth;

use App\Exceptions\GeneralException;
use Hash;
use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @throws GeneralException
     */
    public function rules()
    {

        $input = request()->all();
        $user = access()->user();
        $password_encrypted = $user->password;
        $old_password = $input['old_password'];
        if(Hash::check($old_password, $password_encrypted)){
            //passwords matched
        }
        else{
            throw new GeneralException('password does not match');
        }
        return [
            'old_password' => 'required|string|min:6',
            'password' => 'required|string|min:6|confirmed'
        ];
    }
}
