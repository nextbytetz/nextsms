<?php

namespace App\Http\Requests\Backend\Contact;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property mixed file
 */
class FileImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'required' => 'The :attribute is required and must be of format .xlsx extension'
        ];

    }
}
