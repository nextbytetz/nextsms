<?php
namespace App\Http\Requests\Backend\Contact;



use App\Rules\Backend\Contact\UniquePhoneNumberForLoggedUser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class AddContactRequest extends FormRequest
{


    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [

            'first_name' => [ 'required', 'string', 'max:50'],
            'last_name' => [ 'required', 'string', 'max:50'],
            'phone_number' => [ 'required','phone:TZ'],
        ];

    }
}
