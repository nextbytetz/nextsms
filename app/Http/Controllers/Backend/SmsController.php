<?php
namespace  App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\SMS\sendSmsRequest;
use App\Jobs\Backend\SMS\SendSms;
use App\Models\Auth\User;
use App\Models\Contact\Contact;
use App\Models\Contact\Group;
use App\Models\SMS\Sms;
use App\Models\SMS\SmsRecipient;
use App\Repositories\Backend\Contact\ContactRepository;
use App\Repositories\Backend\Contact\GroupRepository;
use App\Repositories\Backend\SMS\SenderRepository;
use App\Repositories\Backend\SMS\SmsRecipientRepository;
use App\Repositories\SMS\SmsRepository;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;

class SmsController extends Controller
{

    protected $contacts;
    protected $sms;
    protected $senders;
    protected $group;

    /**
     * ContactController constructor
     */
    public function __construct()
    {
        $this->contacts = new ContactRepository;
        $this->sms = new SmsRepository();
        $this->senders = new SenderRepository();
        $this->group  =  new GroupRepository();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.sms.index');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function compose()
    {
        $contacts = $this->contacts->pluckContact();
        $group = $this->group->pluckGroup();
//        dd($group);
        $senders  = $this->senders->senderId();
        return view('backend.sms.compose',compact('contacts','group', 'senders'));
    }

    /**
     * @param sendSmsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function send(sendSmsRequest $request)
    {
        $this->sms->sendSms($request->all());
        return redirect()->route('backend.sms.index');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function sentSmsDataTable()
    {
        $sent = $this->sms->sentSms();

//        $diff = Carbon::now();
        return DataTables::of($sent)
            ->addColumn('time', function ($row)
            {
                return Carbon::parse($row->time)->format('l\\, j F Y\\, H:i').' ('.Carbon::parse($row->time)->diffForHumans().')';
            })
            ->rawColumns(['time'])
            ->make(true);
    }
}
