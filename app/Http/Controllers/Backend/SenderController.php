<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\Backend\SMS\SenderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class SenderController extends Controller
{
    protected $sender;

    public function __construct()
    {
        $this->sender = new SenderRepository();
    }


    /**
     * @throws \Exception
     */
    public function senderIdTable()
    {
        $id = $this->sender->getSenderIds();

        return DataTables::of($id)
        ->make(true);
    }

}
