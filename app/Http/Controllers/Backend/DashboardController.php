<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Auth\UserRepository;
use App\Repositories\Backend\Contact\ContactRepository;
use App\Repositories\SMS\SmsRepository;
use Yajra\DataTables\DataTables;

class DashboardController extends Controller
{

    protected $contacts;
    protected  $users;
    protected $sms;

    public function __construct()
    {
        $this->contacts = new ContactRepository();
        $this->users    = new UserRepository();
        $this->sms      = new SmsRepository();
    }

    public function index()
    {
        $users = $this->users;
        $countNum = $this->contacts->getNumberOfContactsForLoggedUser();
        $countSentSms = $this->sms->countSentSmsByLoggedUser();
        return view('backend.index', compact('countNum','users', 'countSentSms'));
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function usersTable()
    {
        $users = $this->users->all();
        return DataTables::of($users)
            ->addColumn('checkbox', function (){
                return '';
            })
            ->rawColumns(['checkbox'])
            ->make(true);
    }
}
