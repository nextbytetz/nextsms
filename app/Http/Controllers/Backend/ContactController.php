<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Contact\AddContactRequest;
use App\Http\Requests\Backend\Contact\FileImportRequest;
use App\Imports\Backend\ContactsImport;
use App\Models\Contact\Contact;
use App\Repositories\Backend\Contact\ContactRepository;
use App\Rules\Backend\Contact\UniquePhoneNumberForLoggedUser;
use Storage;
use Maatwebsite\Excel\Excel;
use Yajra\DataTables\DataTables;

class ContactController extends Controller
{

    protected $contacts;


    /**
     * ContactController constructor
     * @param ContactRepository $contactRepository
     */
    public function __construct(ContactRepository $contactRepository)
    {
        $this->contacts = $contactRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
//    public function showAddContactForm()
//    {
//        return view('backend.pages.contacts.add');
//    }


    /**
     * @param AddContactRequest $request
     * @return mixed
     * @throws \Throwable
     */
    public function add(AddContactRequest $request)
    {

        $request->validate([
            'phone_number' => [new UniquePhoneNumberForLoggedUser]
        ]);
        $this->contacts->create($request->only('first_name', 'last_name', 'phone_number'));

        return redirect()->route('backend.contacts.all')->withFlashSuccess('Contact Successfully Added');
    }

    /**
     *
     *
     * @param $contact
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function contactsIndex(Contact $contact)
    {
        return view('backend.contacts.all')->with('contact', $contact);
    }


    /**
     * @return mixed
     * @throws \Exception
     */
    public function contactsIndexData()
    {
        $contacts = $this->contacts->getContactsForLoggedUser();

        return DataTables::of($contacts)
//            ->addColumn('checkbox', function ($contact) {
//                return '<input type="checkbox" id="'.$contact->id.'" name="checkbox" />';
//            })
            ->addColumn('actions', function (Contact $contact)
            {
                return $contact->action_buttons;
            })
//            ->addColumn('check', function (Contact $contact)
//            {
//                return $contact->checkbox;
//            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    /**
     * @param Contact $contact
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Contact $contact)
    {
        $dataHTML = view('backend.contacts.includes.edit')->with('contact', $contact);

        return response()->json( 'html', $dataHTML);
    }

    /**
     * @param Contact $contact
     * @param AddContactRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Contact $contact, AddContactRequest $request)
    {
        $input = $request->all();
        //Do the update here
        $this->contacts->update($contact, $input);

        return response()->json(['success' => true]);
    }


    /**
     * @return $this
     */
    public function getEditModal()
    {
        $id = request()->input("id");
        $contact = $this->contacts->query()->where("id", $id)->first();
        return view("backend.contacts.includes.edit_modal")
                ->with("contact", $contact);
    }


    /**
     * @param Contact $contact
     */
    public function destroyContact(Contact $contact)
    {
        $this->contacts->destroyContact($contact->id);
    }

    /**
     * @param FileImportRequest $request
     * @return mixed
     */
    public function upload(FileImportRequest $request)
    {
        $FilePath = Storage::putFile('uploads',$request->file('file'));

        (new ContactsImport)->import($FilePath, null,  Excel::CSV);


//        $csvFilePath = Storage::putFile('uploads',$request->file('csvfile'));
//
//        ImportCsvFile::dispatch($csvFilePath)->onQueue('import');

//        (new ContactsImport())->import($request->file('csvfile'), 'local', Excel::CSV);
//        (new ContactsImport())->import('uploads','local',Excel::XLSX);

        return redirect()->route('backend.contacts.all')->withFlashSuccess('Uploaded Successfully');
    }

}
