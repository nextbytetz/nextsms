<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Contact\AddContactToGroupRequest;
use App\Http\Requests\Backend\Contact\AddGroupRequest;
use App\Models\Contact\Contact;
use App\Models\Contact\Group;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Contact\ContactRepository;
use App\Repositories\Backend\Contact\GroupRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class GroupController extends Controller
{

    protected $group;
    protected $contact;

    /**
     * GroupController constructor.
     */
    public function  __construct()
    {
        $this->contact = new ContactRepository();
        $this->group = new GroupRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        return view('backend.groups.index');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function groups()
    {
        $groups = $this->group->getGroupsForLoggedUser();

        return DataTables::of($groups)
            ->addColumn('checkbox', function (Group $group)
            {
                return $group->checkbox;
            })
            ->addColumn('name', function (Group $group)
            {
                return $group->group_name;
            })
            ->addColumn('actions', function (Group $group)
            {
                return $group->action_buttons;
            })
            ->rawColumns(['actions', 'checkbox', 'name'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param AddGroupRequest $request
     * @return void
     */
    public function create(AddGroupRequest $request)
    {
        $this->group->create($request->all());

        return view('backend.groups.index')->withFlashSuccess('Group Successfully Added');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddContactToGroupRequest $request
     * @param Group $group
     * @return void
     */
    public function storeContact(AddContactToGroupRequest $request, Group $group)
    {

        $this->group->storeContactGroup($group ,$request->all());
        return redirect()->route('backend.group.show', $group)->withFlashSuccess('Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param Group $group
     * @return void
     */
    public function show(Group $group)
    {
//        dd($group);
        return view('backend.groups.show_contacts', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Group $group
     * @return void
     */
    public function edit(Group $group)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Group $group
     * @return void
     */
    public function update(Request $request, Group $group)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Group $group
     * @return void
     */
    public function destroy($group)
    {
//        dd($group);
        $this->group->deleteGroup($group);
        return redirect()->route('backend.groups')->withFlashSuccess('Successfully Deleted');


    }

    public function addContactGroup(Group $group)
    {
        $contacts = access()->user()->contacts->pluck('full_name','id');
        return view('backend.groups.add_contacts', compact('group','contacts'));
    }

    /**
     * @param Group $group
     * @return mixed
     * @throws \Exception
     */
    public function viewGroupDataTable(Group $group)
    {
        $view = $this->group->viewGroupContacts($group);
//        dd($view);
        return DataTables::of($view)
            ->addColumn('remove', function (Contact $contact)
            {
                return $contact->remove_contact;
            })
            ->rawColumns(['remove'])
            ->make(true);
    }

    public function removeContactGroup(Contact $contact, $group_id)
    {
//        dd($group_id);
        $this->group->removeContact($contact->id, $group_id);
    }
}
