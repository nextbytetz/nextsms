<?php
/**
 * Created by PhpStorm.
 * User: reubenwedson
 * Date: 1/18/19
 * Time: 4:05 PM
 */

namespace App\Http\Controllers\Backend;


use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Models\Auth\User;
use App\Repositories\Auth\UserRepository;

class ProfileController
{

    protected $user;

    public  function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function profile()
    {
        $user = access()->user();
        return view('backend.profile.profile')->with('user', $user);
    }

    public function getEditModal()
    {
        $id = request()->input("id");
        $user = $this->user->query()->where("id", $id)->first();
        return view("backend.profile.include.edit_profile")
            ->with("user", $user);
    }

    public function update(User $user, RegisterRequest $request)
    {
        $input = $request->all();
        //Do the update here
        $this->user->update($user, $input);

        return response()->json(['success' => true]);
    }
}
