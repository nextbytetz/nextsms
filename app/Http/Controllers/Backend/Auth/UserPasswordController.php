<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Requests\Backend\Auth\ChangePasswordRequest;
use App\Repositories\Auth\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserPasswordController extends Controller
{
    protected $user;


    public function __construct()
    {
        $this->user = new UserRepository();
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $input = $request->all();
        $this->user->changePassword($input);
        return redirect()->route('backend.sms.profile')->withFlashSuccess('Password changed successfully');
    }
}
