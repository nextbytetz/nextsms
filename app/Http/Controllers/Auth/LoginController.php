<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     * @return string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    protected function guard()
    {
        return Auth::guard('web');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath())->withFlashSuccess(__('alerts.auth.logged-in'));
    }

    public function showLoginForm()
    {
        return view('frontend.index');
    }


    public function username()
    {
        return 'email';
    }

    protected function authenticated(Request $request,User $user)
    {
        if (! $user->isConfirmed()) {
            access()->logout();
            return redirect()->route('auth.verification', $user->uuid)->withFlashWarning(__('alerts.auth.not_confirmed'));
        }
//        elseif (! $user->isActive()) {
//            access()->logout();
//            throw new GeneralException(__('exceptions.auth.deactivated'));
//        }
        Auth::logoutOtherDevices($request->input('password'));
    }

    public function verification(User $user)
    {
        return view("frontend.auth.verify")
            ->with('user', $user);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/')->withFlashSuccess(__('alerts.auth.logged-out'));
    }




}
