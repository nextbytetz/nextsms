<?php

namespace App\Http\Controllers\Auth;

use App\Models\Auth\User;
use App\Notifications\Auth\UserNeedsConfirmation;
use App\Repositories\Auth\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfirmAccountController extends Controller
{

    protected $user;

    /**
     * ConfirmAccountController constructor.
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->middleware('guest')->except('logout');
        $this->user = $user;
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function sendConfirmationEmail(User $user)
    {
        $user->notify(new UserNeedsConfirmation());
        return redirect()->back()->withFlashSuccess(trans('alerts.auth.confirmation_sent'));
    }

    /**
     * @param $token
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function confirm($token)
    {
        $this->user->confirmAccount($token);
        return view('backend.index')->withFlashSuccess(trans('exceptions.auth.confirmation.success'));
    }

}
