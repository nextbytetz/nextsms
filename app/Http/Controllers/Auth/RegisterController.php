<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use Illuminate\Http\FormRequest;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Repositories\Auth\UserRepository;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
    *Protected var
     **/
    protected $user;


    /**
     * RegisterController constructor.
     */
    public function __construct()
    {
        $this->user = new userRepository;
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/
    ';


    public function showRegistrationForm()
    {
        return view('frontend.auth.register');
    }

    /**
     * @param RegisterRequest $request
     * @return mixed
     */
    public function register(RegisterRequest $request)
    {

        $user = $this->user->create($request->all());

        return redirect()->route('auth.registered', $user->uuid)->withFlashSuccess('User Successfully Registered');
    }

    public function showRegisteredForm(User $user){

        return view('frontend.auth.verify')
            ->with('user',$user);

    }

}
