<?php

namespace App\Imports\Backend;

use App\Repositories\Backend\Contact\ContactRepository;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithValidation;
//use Log;
//use Maatwebsite\Excel\Concerns\SkipsOnError;
//use Maatwebsite\Excel\Concerns\SkipsOnFailure;
//use Maatwebsite\Excel\Validators\Failure;
//use Throwable;

class ContactsImport implements ToModel, WithValidation
{

    use Importable;

    /**
     * @param array $row
     *
     * @return void
     * @throws \Throwable
     */
    public function model(array $row)
    {
        $contact = new ContactRepository();
        $data = [
            'first_name' => $row[0],
            'last_name' => $row[1],
            'phone_number' => $row[2],
            'user_id' => access()->id()
        ];
        $contact->create($data);
    }

    public function rules(): array
    {
        return [

            '0' => ['required'],
            '1' => ['required'],
            '2' => ['required', 'phone:TZ'],

        ];
    }

    public function customValidationAttributes()
    {
        return [

            '0' => 'First Name',
            '1' => 'Last Name',
            '2' => 'Phone Number'

        ];
    }

//    /**
//     * @param Throwable $e
//     * @throws Throwable
//     */
//    public function onError(Throwable $e)
//    {
//        throw $e;
//    }
//
//    /**
//     * @param Failure[] $failures
//     */
//    public function onFailure(Failure ...$failures)
//    {
//        Log::info(json_encode($failures));
//    }
}
