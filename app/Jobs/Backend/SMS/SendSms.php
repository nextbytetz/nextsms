<?php

namespace App\Jobs\Backend\SMS;

use App\Models\Contact\Contact;
use App\Models\SMS\SmsRecipient;
use App\Repositories\Backend\Contact\ContactRepository;
use App\Repositories\Backend\SMS\SmsRecipientRepository;
use App\Repositories\SMS\SmsRepository;
use App\Services\SMS\SmsApi;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Redis;


class SendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $sender;

    protected $contact;

    protected $message;

    protected $user;

    /**
     * SendSms constructor.
     * @param $sender
     * @param $contact
     * @param $message
     * @param $user
     */
    public function __construct($sender ,$contact, $message, $user)
    {
        $this->sender  = $sender;
        $this->contact = $contact;
        $this->message = $message;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @param SmsRecipient $smsRecipient
     * @return void
     */
    public function handle(SmsRecipient $smsRecipient)
    {
//        if (env("SEND_SMS", 0)){
            $phone = str_replace("+", "", $this->contact);
//            dd($phone);
            $sms = new SmsApi($this->sender, $phone, $this->message);
            $response = $sms->send();
            $message = new SmsRepository();
            $data = [
                'user_id' => $this->user,
                'body' => $this->message,
                'status' => $response
            ];
            $message->query()->create($data);
            $sms1 = (new SmsRepository())->query()->where("body", $this->message)->first();
            $contact = (new ContactRepository())->query()->where("phone_number", $this->contact)->first();
            $smsRecipient->sms()->associate($sms1->id);
            $smsRecipient->contact()->associate($contact->id);
            $smsRecipient->save();

//        }
    }
}
