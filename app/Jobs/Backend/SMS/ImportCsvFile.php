<?php

namespace App\Jobs\Backend\SMS;

use App\Exceptions\GeneralException;
use App\Repositories\Backend\Contact\ContactRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Storage;


class ImportCsvFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $csvFilePath;

    /**
     * Create a new job instance.
     *
     * @param $csvFilePath
     */
    public function __construct($csvFilePath)
    {
        $this->csvFilePath = $csvFilePath;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws GeneralException
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     * @throws \Throwable
     */
    public function handle()
    {

        if (!Storage::disk('local')->exists($this->csvFilePath))
            throw new GeneralException(sprintf(
                'ERROR: "%1$s"'
                , $this->csvFilePath
            ));


        $fileData = Storage::get($this->csvFilePath);
        $csvData = str_getcsv($fileData, "\n");

        foreach ($csvData as $row) {
            $row = str_getcsv($row, ",");

            if(is_array($row) && sizeof($row) == 3) {
                $contact = new ContactRepository();
                $data = [
                    'first_name' => $row[0],
                    'last_name' => $row[1],
                    'phone_number' => $row[2],
                    'user_id' => access()->id()
                ];
                $contact->create($data);
            }
        }
    }
}
