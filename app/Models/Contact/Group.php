<?php

namespace App\Models\Contact;

use App\Models\Contact\Traits\Attribute\GroupAttribute;
use App\Models\Contact\Traits\Relationship\GroupRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 *@property mixed id
 * @property mixed action_buttons
 * @property mixed contact
 * @property mixed group_name
 * @property mixed remove_contact
 * @property mixed checkbox
 */
class Group extends Model
{
    use GroupRelationship,
        GroupAttribute;

    protected $guarded = [];


    protected $dates = [
        'create_at', 'updated_at'
    ];

    public function getRouteKeyName()
    {
        return 'name';
    }

}
