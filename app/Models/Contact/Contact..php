<?php
namespace App\Models\Contact;

use App\Models\Contact\Traits\ContactRelationship;
use App\Models\Contact\Traits\Attribute\ContactAttribute;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed action_buttons
 * @property mixed checkbox
 * @property mixed id
 * @property mixed full_name
 * @property mixed delete_button
 * @property mixed remove_contact
 */
class Contact extends Model
{
    use ContactRelationship,
        ContactAttribute;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $guarded = [];


    protected $dates = [
        'create_at', 'updated_at'
    ];


//    public function getRouteKeyName()
//    {
//        return 'first_name'.'_'.'last_name';
//    }


}
