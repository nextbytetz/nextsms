<?php
/**
 * Created by PhpStorm.
 * User: reubenwedson
 * Date: 2/8/19
 * Time: 1:51 PM
 */

namespace App\Models\Contact\Traits\Relationship;


use App\Models\Auth\User;
use App\Models\Contact\Contact;

trait GroupRelationship
{

    public function contact()
    {
        return $this->belongsToMany(Contact::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
