<?php
namespace App\Models\Contact\Traits;

use App\Models\Auth\User;
use App\Models\Contact\Group;
use App\Models\SMS\SmsRecipient;

trait ContactRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sms_recipient()
    {
        return $this->hasMany(SmsRecipient::class,'contact_id');
    }

    public function group()
    {
        return $this->belongsToMany(Group::class);
    }
}
