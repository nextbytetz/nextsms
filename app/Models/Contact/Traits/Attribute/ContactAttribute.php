<?php
namespace App\Models\Contact\Traits\Attribute;


use App\Models\Contact\Group;

trait ContactAttribute
{

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {

        return link_to('contact/edit/' . $this->id, '', ['type'=>'i', 'class' => 'fa fa-pencil edit_contact', 'alt' => 'Edit', 'data-method'=>'edit_contact', 'data-target-id' => $this->id, 'data-toggle'=> 'tooltip', 'data-placement'=> 'top',  'title'=> 'Edit']);
    }

    public function getDeleteButtonAttribute()
    {

        return link_to('contact/delete/' . $this->id, '', [ 'type'=>'i','class' => 'fa fa-trash-o delete_contact','data-target-id' => $this->id, 'data-toggle'=> 'tooltip', 'data-placement'=> 'top',  'title'=> 'Delete']);

    }
        public function getActionButtonsAttribute()
    {
        return $this->getEditButtonAttribute() . ' ' . $this->getDeleteButtonAttribute() ;
    }

    public function getCheckboxAttribute()
    {
        return link_to('contact/delete/'. $this->id, ['type'=>'checkbox', 'class' => 'icheck2 skin']);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute(){
        return ucfirst($this->attributes['first_name'].' '. $this->attributes['last_name']);
    }

    /**
     * @return string
     */
    public function getRemoveContactAttribute()
    {

        return link_to('remove/'.$this->id.'/group/'.$this->group()->where('contact_id',$this->id)->first()->id, '', ['type' => 'i', 'class' => 'fa fa-trash-o delete_contact', 'data-target-id' => $this->id, 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Remove']);

    }

}
