<?php
/**
 * Created by PhpStorm.
 * User: reubenwedson
 * Date: 2/11/19
 * Time: 10:51 AM
 */

namespace App\Models\Contact\Traits\Attribute;


trait GroupAttribute
{

    public function getDeleteButtonAttribute()
    {
        return link_to('delete/'.$this->id, '', [ 'type'=>'i','class' => 'fa fa-trash-o delete_group', 'data-toggle'=> 'tooltip', 'data-placement'=> 'top',  'title'=> 'Delete']);
    }


    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return link_to('group/edit/' . $this->id, '', ['type'=>'i', 'class' => 'fa fa-pencil edit_group', 'title' => 'Edit', 'data-method'=>'edit_group', 'data-target-id' => $this->id]);
    }

    public function getViewGroupButtonAttribute()
    {
//        return link_to_route('backend.contacts.group.show', '', [$this->id], ['type' => 'i', 'class' => 'fa fa-eye', 'data-method'=>'edit_group', 'data-target-id' => $this->id]);
        return '<a href="'.route('backend.group.show', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'"><i class="fa fa-eye"></i></a>';
    }

    public function getAddContactButtonAttribute()
    {
//        return link_to_route('backend.contacts.group.add.contacts', '',[$this->id], ['type' => 'i', 'class' => 'fa fa-user-plus', 'data-method'=>'edit_group', 'data-target-id' => $this->id]);
        return '<a href="'.route('backend.group.add.contacts', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.add').'"><i class="fa fa-user-plus"></i></a>';
    }


    public function getActionButtonsAttribute()
    {
        return  $this->getAddContactButtonAttribute().' '.$this->getViewGroupButtonAttribute() .' '. $this->getEditButtonAttribute() .' '. $this->getDeleteButtonAttribute()  ;
    }

    public function getGroupNameAttribute()
    {
        if($this->contact()->count() >= 1)
        {
            return $this->attributes['name'].' '.'<span class="badge badge-secondary">'.$this->contact()->count().' contact(s)'.'</span>';
        }
        else{
            return $this->attributes['name'].' '. '<span class="badge badge-danger">'.$this->contact()->count().' contact(s)'.'</span>';
        }
    }


    public function getRemoveContactAttribute()
    {
        return link_to('group/remove/' . $this->id, '', [ 'type'=>'i','class' => 'fa fa-trash-o delete_group','data-target-id' => $this->id, 'data-toggle'=> 'tooltip', 'data-placement'=> 'top',  'title'=> 'Remove']);
    }


    public function getCheckboxAttribute()
    {
        return '<input type="checkbox" class="sub" data-id="'.$this->id.'">';
    }

}
