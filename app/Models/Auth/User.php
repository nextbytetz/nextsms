<?php

namespace App\Models\Auth;

use App\Models\Auth\Traits\UserAccess;
use App\Models\Auth\Traits\Attribute\UserAttribute;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Models\Auth\Traits\Relationship\UserRelationship;
use Webpatser\Uuid\Uuid;

/**
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed full_name
 * @property mixed email
 * @property mixed id
 * @property mixed uuid
 */
class User extends Authenticatable
{
    use Notifiable,
        CanResetPassword,
        UserAccess,
        UserAttribute,
        UserRelationship;


    protected $guarded = [];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }
}
