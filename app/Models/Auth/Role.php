<?php

namespace App\Models\Auth;

use App\Models\Auth\Traits\RoleAccess;
use App\Models\Auth\Relationship\RoleRelationship;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use RoleRelationship,
        RoleAccess;
}
