<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\Permission;
use App\Models\Auth\Role;
use App\Models\Contact\Contact;
use App\Models\Contact\Group;
use App\Models\SMS\Sender;
use App\Models\SMS\Sms;

trait UserRelationship
{
    public function contacts()
    {
        return $this->hasMany(Contact::class, 'user_id');
    }

    public function sms()
    {
        return $this->hasMany(Sms::class,'user_id');
    }

    public function senders()
    {
        return $this->belongsToMany(Sender::class);
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @return mixed
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class)->withTimestamps();
    }
}
