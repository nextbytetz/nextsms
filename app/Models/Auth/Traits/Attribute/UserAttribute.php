<?php


namespace App\Models\Auth\Traits\Attribute;


use App\Models\Auth\User;

trait UserAttribute
{
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * @return bool
     */
    public function getIsAdminAttribute()
    {

        foreach ($this->roles()->get() as $role) {
            if ($role->id == 5) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->confirmed == 'true';
    }

}
