<?php
namespace App\Models\SMS\Traits\Relationship;

use App\Models\Contact\Contact;
use App\Models\SMS\Sms;

trait SmsRecipientRelationship
{
    /**
     * @return mixed
     */
    public function sms()
    {
        return $this->belongsTo(Sms::class);
    }

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }
}
