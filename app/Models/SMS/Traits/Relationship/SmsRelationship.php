<?php
namespace App\Model\SMS\Traits\Relationship;

use App\Models\Auth\User;
use App\Models\SMS\SmsRecipient;

trait SmsRelationship
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sms_recipient()
    {
        return $this->hasMany(SmsRecipient::class, 'sms_id');
    }
}
