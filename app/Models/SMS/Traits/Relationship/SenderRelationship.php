<?php
/**
 * Created by PhpStorm.
 * User: reubenwedson
 * Date: 3/5/19
 * Time: 11:59 AM
 */

namespace App\Models\SMS\Traits\Relationship;


use App\Models\Auth\User;

trait SenderRelationship
{
    public function users()
    {
     return $this->belongsToMany(User::class);
    }
}
