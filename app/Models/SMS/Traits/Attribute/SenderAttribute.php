<?php
/**
 * Created by PhpStorm.
 * User: reubenwedson
 * Date: 3/5/19
 * Time: 11:20 AM
 */

namespace App\Models\SMS\Traits\Attribute;


trait SenderAttribute
{
    /**
     * @return mixed
     */
    public function getSenderIdAttribute()
    {
        return $this->attributes['name'];
    }
}
