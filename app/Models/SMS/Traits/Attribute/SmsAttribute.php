<?php
/**
 * Created by PhpStorm.
 * User: reubenwedson
 * Date: 2/25/19
 * Time: 3:23 PM
 */

namespace App\Models\SMS\Traits\Attribute;

use Carbon\Carbon;

trait SmsAttribute
{

    public function getSentSmsTimeAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }

}
