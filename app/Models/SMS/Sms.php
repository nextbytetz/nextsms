<?php
namespace App\Models\SMS;

use App\Model\SMS\Traits\Relationship\SmsRelationship;
use App\Models\SMS\Traits\Attribute\SmsAttribute;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed sent_sms_time
 */
class Sms extends Model
{
    use SmsRelationship,
        SmsAttribute;

    protected $table = 'sms';

    protected $guarded = [];

    protected $dates = [
        'create_at', 'updated_at'
    ];

}
