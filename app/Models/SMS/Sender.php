<?php

namespace App\Models\SMS;

use App\Models\SMS\Traits\Attribute\SenderAttribute;
use App\Models\SMS\Traits\Relationship\SenderRelationship;
use Illuminate\Database\Eloquent\Model;

class Sender extends Model
{
    use SenderAttribute,
        SenderRelationship;

    protected $guarded = [];
}
