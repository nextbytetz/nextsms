<?php
namespace App\Models\SMS;

use App\Models\SMS\Traits\Relationship\SmsRecipientRelationship;
use Illuminate\Database\Eloquent\Model;

class SmsRecipient extends Model
{
    use SmsRecipientRelationship;

    protected $table = 'sms_recipients';

    protected $guarded = [];

    protected $dates = [
        'create_at', 'updated_at'
    ];
}
