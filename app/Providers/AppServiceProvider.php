<?php

namespace App\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;
use Laravel\Horizon\Horizon;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//
//        Horizon::auth(function ($request) {
//            // Always show admin if local development
//            if (env('APP_ENV') == 'local') {
//                return true;
//            }
//        });

        Blade::if('Admin', function () {
        return access()->check() && access()->user()->is_admin;
    });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


}
