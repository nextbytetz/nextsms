<?php

namespace App\Providers;

use App\Services\Access\Access;
use App\Services\Access\Facades\Access as AccessFacade;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;




class AccessServiceProvider extends ServiceProvider
{

    protected $defer = false;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerAccess();
        $this->registerFacade();
    }

    private function registerAccess()
    {
        $this->app->bind('access', function () {
            return new Access();
        });
    }

    private function registerFacade()
    {
        $this->app->booting(function () {
            $loader = AliasLoader::getInstance();
            $loader->alias('Access', AccessFacade::class);
        });
    }
}
