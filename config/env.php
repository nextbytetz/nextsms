<?php
/**
 * Created by PhpStorm.
 * User: gwanchi
 * Date: 9/14/18
 * Time: 9:09 AM
 */

return [
    "key" => "PscUNdlxRk8Q3nwM",
    "app" => [
        "name" => "NextSMS",
        "company" => "TMEA | TPSF",
        "vendor" => "NEXTBYTE ICT SOLUTIONS CO LTD",
        "vendor_web" => "https://www.nextbyte.co.tz",
        "client_web" => "https://tpsftz.org/",
        "description" => "NFLIP PORTAL",
        "keywords" => "",
    ],
];
